import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor() { }

  nameCtr: AbstractControl;
  phoneCtr: AbstractControl;
  emailCtr: AbstractControl;
  messageCtr: AbstractControl;
  contactUsFormGroup: FormGroup;
  invalidSubmission: boolean
  ngOnInit() {
    this.contactUsFormGroup = new FormGroup({
      'nameCtr': new FormControl("", [Validators.required]),
      'phoneCtr': new FormControl("", [Validators.pattern('^[0-9]+$'), Validators.required]),
      'emailCtr': new FormControl("", [Validators.required]),
      'messageCtr': new FormControl("", [Validators.required])
    });

    this.invalidSubmission=false;
    this.nameCtr = this.contactUsFormGroup.get('nameCtr');
    this.phoneCtr = this.contactUsFormGroup.get('phoneCtr');
    this.emailCtr = this.contactUsFormGroup.get('emailCtr');
    this.messageCtr = this.contactUsFormGroup.get('messageCtr');
  }

  onSubmit()
  {
    
  }
}
