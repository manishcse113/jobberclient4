import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AlertComponent } from './_directives/alert.component';
import { LoginComponent } from './login/login.component';

import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './_guards';
import { ServicesComponent } from './services/services.component';
import { UserJobsContainerComponent } from './user-jobs-container/user-jobs-container.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LoggedInComponent } from './loggedIn/loggedIn.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConsumerProfileComponent } from './login/consumer-profile/consumer-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AppComponent } from './app.component';

const appRoutes: Routes = [

    {path: '', component: AlertComponent},
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },    
    { path: 'services', component: ServicesComponent },
    { path: 'user-jobs-container', component: UserJobsContainerComponent},
    { path: 'loginRe', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'forgetPassword', component: ForgetPasswordComponent },
    { path: 'loggedIn', component: LoggedInComponent},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'ConsumerProfile', component: ConsumerProfileComponent},
    { path: 'ResetPassword', component: ResetPasswordComponent},
    { path: 'MyHome', component: AppComponent},


   

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
