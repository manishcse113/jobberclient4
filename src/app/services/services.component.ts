import { Component, OnInit } from '@angular/core';
import {Services} from './mock-services';
import {Service} from './service';
import {MatGridListModule} from '@angular/material/grid-list';
//import {GridLayout} from "ui/layouts/grid-layout";
import {NgGridModule, NgGrid, NgGridItem, NgGridConfig, NgGridItemConfig, NgGridItemEvent} from 'angular2-grid';
//import { NgGridConfig } from 'angular2-grid/interfaces/INgGrid';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  //code added below for services:
  providedServices = Services;
  
  selectedService: Service = this.providedServices[0];
    
    onSelect(service: Service): void {
      this.selectedService = service;
      
    }

  constructor() { }

  ngOnInit() {
  }

  private gridConfig: NgGridConfig = <NgGridConfig>{
		'margins': [5],
		'draggable': true,
		'resizable': true,
		'max_cols': 0,
		'max_rows': 0,
		'visible_cols': 0,
		'visible_rows': 0,
		'min_cols': 1,
		'min_rows': 1,
		'col_width': 2,
		'row_height': 2,
		'cascade': 'up',
		'min_width': 50,
		'min_height': 50,
		'fix_to_grid': false,
		'auto_style': true,
		'auto_resize': false,
		'maintain_ratio': false,
		'prefer_new': false,
		'zoom_on_drag': false,
		'limit_to_screen': true
	};
}
