import { Injectable } from '@angular/core';
import { HttpModule, Response, RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { ServiceProvider } from "../service-provider/ServiceProvider";
import {domain, EnquiryMsgResponse} from "./../APISymbols";
import { JobCommentCreateResponse, JobCommentPost, JobCommentsGetResponse, JobsGetResponse, CommentsGetResponse, CommentCreateResponse, CommentPost, JobCreateResponse, JobPost, EnquiriesByUserResponse, EnquiryMsg, SearchServicenManResult, SearchAllServiceManResult, SearchServiceManMsg, FavouriteServiceManResult, AddToFavouriteResponse, UserServiceManMap,RemoveFromFavouriteResponse, httpPostOptions} from "./../APISymbols" 
import { UserService } from '../_services/user.service';
import { User } from '../_models';
import { timestamp } from 'rxjs/internal/operators/timestamp';

@Injectable()
export class ServiceProviderService {
   constructor(private http: HttpClient, private loginService: UserService) {
     this.http=http;
    }

   //urlServiceman : String;
   urlServiceman = domain + 'SearchServiceMan';
   urlFavouriteServieman = domain + 'SmanUserMap';
   urlEnquiryToServieman = domain + 'Enquiry';
   urlSearchAllServiceman = domain + "SearchAll"
   urlEnquiriesByUser = domain + 'EnquiryByUser';

   urlJob = domain + 'Job';
   urlPostComment = domain + 'EnquiryResponse';
   urlCommentsByEnquiryId = domain + 'EnquiryResponses';

   urlJobUpdate = domain + 'JobUpdate';
   urlJobsByMe = domain + 'JobsByUser';
   urlJobsByCity = domain + 'JobsByCity';
   urlJobsByCityServiceName = domain + 'JobsByCityService';
   urlCommentsByJobId = domain + 'ResponsesByJob';
   urlJobPostComment = domain + 'Response';

   //city: "Gurgaon";
   //service = "Electrician";

   getServiceMan(): Observable<ServiceProvider[]> 
   {
       let timestamp =  +new Date();
       
       let headers = new Headers();
       headers.append("Content-Type", "application/x-www-form-urlencoded");
       headers.append('Accept', 'application/json');
        
       let requestOptions = new RequestOptions({ headers: headers});

       //let http : HttpClient;
       console.log("getServiceMen");
       //return this.http.get(this.urlServiceman + this.city + "/" + this.service)
       return this.http.get<ServiceProvider[]> (this.urlServiceman +"/")
       //  .map((res: Response) =>  res.json())         
       //  .catch(error =>  Observable.throw(error.json().error|| 'Server Error')
       //  ); 
   }

   getNearByServiceManCity(service: String, city: Observable<string>): Observable<SearchServicenManResult> 
   {
    return city.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(city => this.getNearByServiceMan(city, service));
   }

   getNearByServiceMan(city: String, service: String): Observable<SearchServicenManResult> 
   {
       let timestamp =  +new Date();
       let headers = new Headers();
       headers.append("Content-Type", "application/x-www-form-urlencoded");
       headers.append('Accept', 'application/json');
       let requestOptions = new RequestOptions({ headers: headers});
       console.log("getNearByServiceMan");
       return this.http.get<SearchServicenManResult> (this.urlServiceman +"Prof/" + city + "/" + service)
   }

   getFavouriteServiceMan(): Observable<FavouriteServiceManResult> 
   {
       let timestamp =  +new Date();
       let headers = new Headers();
       headers.append("Content-Type", "application/x-www-form-urlencoded");
       headers.append('Accept', 'application/json');
       let requestOptions = new RequestOptions({ headers: headers});
       console.log("getFavouriteServiceMan");
       let currentUser : User;
       currentUser = this.loginService.getCurrentUser();
       return this.http.get<FavouriteServiceManResult> (this.urlFavouriteServieman + "/"+currentUser.id);
   }

   getAllServiceMan(): Observable<SearchAllServiceManResult> 
   {
       let timestamp =  +new Date();
       let headers = new Headers();
       headers.append("Content-Type", "application/x-www-form-urlencoded");
       headers.append('Accept', 'application/json');
       let requestOptions = new RequestOptions({ headers: headers});
       console.log("SearchAllServiceManResult");
       let currentUser : User;
       currentUser = this.loginService.getCurrentUser();
       return this.http.get<SearchAllServiceManResult> (this.urlSearchAllServiceman);
   }

   addtoFavourite(smanId: number): Observable<AddToFavouriteResponse> {
    let currentUser : User;
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convret " + currentUser.id)
     let mapping: UserServiceManMap = {userId: currentUser.id, smanId: smanId}; //+ converts string to number
     return this.http.post<AddToFavouriteResponse>(this.urlFavouriteServieman + "/", mapping, httpPostOptions);
  }

  removeFromFavourite(smanId: number): Observable<RemoveFromFavouriteResponse> {
    let currentUser : User;
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
    // let mapping: UserServiceManMap = {userId: currentUser.id, smanId: smanId}; //+ converts string to number
    return this.http.delete<RemoveFromFavouriteResponse> (this.urlFavouriteServieman + "/"+currentUser.id + "/" + smanId);
     
     //return this.http.delete<AddToFavouriteResponse>(this.urlFavouriteServieman + "/", mapping, httpPostOptions);
  }

  sendEnquiry(smanId: number, enquiryMsg: string): Observable<EnquiryMsgResponse> {
    let currentUser : User;
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
     let mapping: EnquiryMsg = {userId: currentUser.id, smanId: smanId, msg: enquiryMsg}; //+ converts string to number
     return this.http.post<EnquiryMsgResponse>(this.urlEnquiryToServieman + "/", mapping, httpPostOptions);
  }

  getEnquiriesByUser(): Observable<EnquiriesByUserResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getEnquiriesByUser");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<EnquiriesByUserResponse> (this.urlEnquiriesByUser + "/" + currentUser.id);
  }

  postJob(city: string, location:string, serviceName: string, title: string): Observable<JobCreateResponse> {
    let currentUser : User;
    let timestamp =  +new Date();
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
     let mapping: JobPost = {userId: currentUser.id, city: city, location: location, phone: currentUser.phone, 
                            title: title, serviceName:serviceName, date: Date.now().toString(), time: timestamp.toString(),
                            status:'Open'
                        }; //+ converts string to number
     return this.http.post<JobCreateResponse>(this.urlJob + "/", mapping, httpPostOptions);
  }

  updateJob(jobId:string, city: string, location:string, serviceName: string, title: string, status: string): Observable<JobCreateResponse> {
    let currentUser : User;
    let timestamp =  +new Date();
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
     let mapping: JobPost = {userId: currentUser.id, city: city, location: location, phone: currentUser.phone, 
                            title: title, serviceName:serviceName, date: Date.now().toString(), time: timestamp.toString(),
                            status:status
                        }; //+ converts string to number
     return this.http.post<JobCreateResponse>(this.urlJobUpdate + "/" + jobId, mapping, httpPostOptions);
  }

  postComment(enquiryId: string, comment:string, parentResponseId: string): Observable<CommentCreateResponse> {
    let currentUser : User;
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
    let timestamp =  +new Date();
    var senderId = currentUser.id;
    var senderUserName = currentUser.userName;
    var senderPhone = currentUser.phone;
    var senderFirstName = currentUser.firstName.toString();
    var isConsumer = false;
    if (currentUser.userType=="Consumer")
    {
        isConsumer=true;
    }
    var msg: string = comment;
    
    let mapping: CommentPost = {senderId: senderId, enquiryId, senderUserName, senderPhone, senderFirstName,
        isConsumer, msg, parentResponseId }; //+ converts string to number
    return this.http.post<CommentCreateResponse>(this.urlPostComment + "/", mapping, httpPostOptions);
  }

  getEnquiryComments(enquiryId: string): Observable<CommentsGetResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getEnquiryComments");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<CommentsGetResponse> (this.urlCommentsByEnquiryId + "/" + enquiryId);
  }

  getJobsByMe(): Observable<JobsGetResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getJobsByMe");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<JobsGetResponse> (this.urlJobsByMe + "/" + currentUser.id);
  }

  getJobsByCity(city: string): Observable<JobsGetResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getJobsByCity");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<JobsGetResponse> (this.urlJobsByCity + "/" + city);
  }

  getJobsByCityServiceName(city: string, serviceName: string): Observable<JobsGetResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getJobsByCityServiceName");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<JobsGetResponse> (this.urlJobsByCityServiceName + "/" + city + "/" + serviceName);
  }

  getJobComments(jobId: string): Observable<JobCommentsGetResponse> 
  {
      let timestamp =  +new Date();
      let headers = new Headers();
      headers.append("Content-Type", "application/x-www-form-urlencoded");
      headers.append('Accept', 'application/json');
      let requestOptions = new RequestOptions({ headers: headers});
      console.log("getJobComments");
      let currentUser : User;
      currentUser = this.loginService.getCurrentUser();
      return this.http.get<JobCommentsGetResponse> (this.urlCommentsByJobId + "/" + jobId);
  }

  postJobComment(jobId: string, comment:string, parentResponseId: string): Observable<JobCommentCreateResponse> {
    let currentUser : User;
    currentUser = this.loginService.getCurrentUser();
    console.log ("#########ID is " + currentUser.id +" convert " + currentUser.id)
    let timestamp =  +new Date();
    var senderId = currentUser.id;
    var senderUserName = currentUser.userName;
    var senderPhone = currentUser.phone;
    var senderFirstName = currentUser.firstName.toString();
    var isConsumer = false;
    if (currentUser.userType=="Consumer")
    {
        isConsumer=true;
    }
    var msg: string = comment;
    
    let mapping: JobCommentPost = {senderId: senderId, jobId, senderUserName, senderPhone, senderFirstName,
        isConsumer, msg, parentResponseId }; //+ converts string to number
    return this.http.post<JobCommentCreateResponse>(this.urlJobPostComment + "/", mapping, httpPostOptions);
  }

}