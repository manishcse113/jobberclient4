import { Service } from './service';

export const Services: Service[] = [

  
  { id: 1, name: 'Electrician', serviceIconPath: './assets/images/electrician.png' },
  { id: 2, name: 'Plumber' , serviceIconPath: './assets/images/plumber.png' },
  { id: 3, name: 'Gardener' , serviceIconPath: './assets/images/gardener.png' },
  { id: 4, name: 'Carpenter', serviceIconPath: './assets/images/carpenter.png'  },
  { id: 5, name: 'Painter', serviceIconPath: './assets/images/painter.png'  },
  { id: 6, name: 'Driver', serviceIconPath: './assets/images/driver.png'  },
  { id: 7, name: 'Maid', serviceIconPath: './assets/images/maid.png'  },
  { id: 8, name: 'Cook', serviceIconPath: './assets/images/cook.png'  },
  { id: 9, name: 'Nurse', serviceIconPath: './assets/images/nurse.png'  },
  { id: 10, name: 'CarMechanic', serviceIconPath: './assets/images/car_mechanic.png'  },
  { id: 11, name: 'BikeMechanic', serviceIconPath: './assets/images/bike_mechanic.png'  },
  { id: 12, name: 'Laundry', serviceIconPath: './assets/images/laundry.png'  },
  
];