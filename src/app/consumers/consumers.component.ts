import { Component} from '@angular/core';
import { OnInit } from '@angular/core';
import { Consumer } from './consumer';
import { Consumers } from './mock-consumers';

@Component({
  selector: 'app-consumers',
  templateUrl: './consumers.component.html',
  styleUrls: ['./consumers.component.css']
})
export class ConsumersComponent implements OnInit {
 consumers = Consumers;

  selectedConsumer: Consumer;

  onSelect(consumer: Consumer): void {
    this.selectedConsumer = consumer;
  }

  constructor() { }

  ngOnInit() {
  }

}
