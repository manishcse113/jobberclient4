import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpMapComponent } from './sp-map.component';

describe('SpMapComponent', () => {
  let component: SpMapComponent;
  let fixture: ComponentFixture<SpMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
