// just an interface for type safety.
import {ServiceProvider} from '../service-provider/ServiceProvider' ;

export class MapMarker {
  lat: number;
  lng: number;
  title: String;
  label: String;
  icon: String;
  infoable:boolean;
  clickable: boolean;
  sp: ServiceProvider;
  
  constructor(
    lat: number,
    lng: number,
    title: String,
    label: String,
    icon: String,
    infoable:boolean,
    clickable: boolean,
    sp: ServiceProvider
  )
  {
    this.lat = lat;
    this.lng = lng;
    this.label = label;
    this.title = title;
    this.icon = icon;
    this.infoable = infoable;
    this.clickable = clickable;   
    this.sp=sp; 
  }
  //draggable: boolean;
}