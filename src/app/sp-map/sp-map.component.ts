import { Component, OnInit } from '@angular/core';

import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

import {MapMarker} from './mapMarker';
import {ServiceProvider} from '../service-provider/ServiceProvider';
import {SearchServicenManResult} from '../APISymbols';

@Component({
  selector: 'app-sp-map',
  templateUrl: './sp-map.component.html',
  styleUrls: ['./sp-map.component.css']
})

export class SpMapComponent implements OnInit {


  public latitude: number = 38;
  public longitude: number = 38;
  public searchControl: FormControl;
  public zoom: number = 3;
  markers: MapMarker[] = [];

  @ViewChild("search2")
  private searchElementRef2: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
    ) 
    {

    }

  ngOnInit() {

     //set google maps defaults
     this.zoom = 4;
     this.latitude = 39.8282;
     this.longitude = -98.5795;
 
     this.markers =[];
 
     //create search FormControl
     this.searchControl = new FormControl();
 
     //set current position
     //this.setCurrentPosition(); //to uncomment
 
     //load Places Autocomplete
     this.mapsAPILoader.load().then(() => {
      /*
       let autocomplete = new google.maps.places.Autocomplete
        (this.searchElementRef2.nativeElement, {
         types: ["address"]
        });
       autocomplete.addListener("place_changed", () => {
         this.ngZone.run(() => {
           //get the place result
           let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
           //verify result
           if (place.geometry === undefined || place.geometry === null) {
             return;
           }
           //set latitude, longitude and zoom
           this.latitude = place.geometry.location.lat();
           this.longitude = place.geometry.location.lng();
           this.zoom = 12;
         });
       });
       */
      this.latitude = 23.50;
      this.longitude = 78.45;
      this.zoom = 12;
     });
  }

  lat: number;// = 51.678418;
  lng: number;// = 7.809007;
  private setCurrentPosition() 
  {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 13;
      });
    }
  }
  
  public setPositionForFirstMarker()
  {
    if (this.markers.length>0)
    {
      this.latitude = this.markers[0].lat;
      this.longitude = this.markers[0].lng;
      this.zoom = 13;
    }
    else
    {
     //this.setCurrentPosition();  
    }
  }
  public clearAllMarkers()
  {
    this.markers =[];
    //this.setPositionForFirstMarker();//to uncomment
  }

  private nearbyIcon: String = './../../assets/images/nearbyMarker.png';
  private favIcon: String = './../../assets/images/myfavMarker.png';
  private respIcon: String = './../../assets/images/myrespMarker.png';

  public addMarker(sp: ServiceProvider, index:number, markerType: number)
  {
    if (markerType==0)
    { this.markers[index] = new MapMarker( sp.lat, sp.lng, sp.userName, sp.serviceName, this.nearbyIcon, true,true,sp );}
    else if (markerType==1)
    { this.markers[index] = new MapMarker( sp.lat, sp.lng, sp.userName, sp.serviceName, this.favIcon, true,true,sp );}
    else if (markerType==2)
    { this.markers[index] = new MapMarker( sp.lat, sp.lng, sp.userName, sp.serviceName, this.respIcon, true,true,sp );}
    else
    { this.markers[index] = new MapMarker( sp.lat, sp.lng, sp.userName  , sp.serviceName, this.nearbyIcon, true,true,sp );}
  }

  public addMarkersToMap (serviceProviders: SearchServicenManResult[], markerType: number)
  {
    for(let i=0; i< serviceProviders.length; i++)
    {
      //this.addMarker(serviceProviders[i].sman,i, markerType);//to uncomment
    }
  }

  geocoder: google.maps.Geocoder;
  /*
    addServiceManToMap(serviceProviders: ServiceProvider[])
  {
    let i=0;
    this.markers = [];
    for(let i=0; i< serviceProviders.length; i++)
    {
      let address: String = serviceProviders[0].location + " " + serviceProviders[0].city; 
      this.addMarkerToMap(address, serviceProviders[i]);
    }
  }

  addMarkerToMap(address, serviceProvider: ServiceProvider)
  {
    let index = this.markers.length;
    let marker :MapMarker;
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'Ardee City, Gurgaon, India';
    // Initialize the Geocoder
    this.geocoder = new google.maps.Geocoder();
    if (this.geocoder) 
    {
        this.geocoder.geocode(
          {
            'address': address
          },
          (results)=> 
          //function (results, status) 
          {
              //if (status == google.maps.GeocoderStatus.OK) 
              {
               //this.markers.push( {lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng(), label: serviceProvider.name, title: serviceProvider.serviceName, icon: serviceProvider.serviceIconPath, infoable: true, clickable: true }); 
                marker = new MapMarker( results[0].geometry.location.lat(), results[0].geometry.location.lng(), serviceProvider.name, serviceProvider.serviceName, serviceProvider.serviceIconPath, true,true );
                this.markers[index]= marker;
              }
          }
        );
    }
  }
  */

  selectedMarker:MapMarker;
  selectedSP:ServiceProvider;

  markerClicked(marker:MapMarker)
  {
   //console.log('Clicked Marker:'+ marker.label);
   this.selectedMarker=marker;
   this.selectedSP = this.selectedMarker.sp;
  }

  mouseOver(marker:MapMarker)
  {
    //console.log('Over Marker:'+ marker.label);
    this.selectedMarker=marker;
    this.selectedSP = this.selectedMarker.sp;
  }
}
