import {HttpHeaders} from '@angular/common/http';
//import { ExecFileOptionsWithStringEncoding } from 'child_process';
import {ServiceProvider} from './service-provider/ServiceProvider'

export const domain = 'http://52.55.10.96:8086/';
export const httpPostOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export const userTypeConsumer="Consumer";
export const userTypeServiceMan = "ServiceMan";
export const subPathTypeProfile = "/profile/";

export const httpPostOptionsUpload = {
  headers: new HttpHeaders({
   // 'Content-Type': 'multipart/form-data'
  })
};

export interface Login {
  _id: number;
  updatedAt: string;
  createdAt: string;
  photoUrl: string;
  isVerified: boolean;
  lastName: string;
  firstName: string;
  pin:string;
  location:string;
  city:string;
  state:string;
  email:string;
  phone:string;
  userName: string;
  __v:number;
  serviceName: string;
}

export interface LoginMsg {
  message: string;
  result?: Login[];
}

export interface ProfileMsg {
  message: string;
  consumer: Login;
}

export interface fileUpload {
}

export interface likingUser {
  userId: string;
}

export interface SearchServiceManMsg {
  message: string;
  smen?: SearchServicenManResult[];
}


export interface SearchServicenManResult {
  message: string;
  smen: ServiceProvider[];
}


export interface FavouriteServiceManResult {
  message: string;
  smen: ServiceProvider[];
}

export interface SearchAllServiceManResult {
  message: string;
  smen: ServiceProvider[];
}

export interface AddToFavouriteResponse {
  message: string;
  mapping?: UserServiceManMap;
}

export interface RemoveFromFavouriteResponse {
  message: string;
  //mapping?: UserServiceManMap;
}

export interface UserServiceManMap {
    userId: number; 
    smanId: number;
  }

export interface EnquiryMsg {
  userId: number; 
  smanId: number;
  msg: string;
}

export interface EnquiryMsgResponse {
  message: string;
  result?: enquiryResult;
}

export interface enquiryResult {
  __v : string;
  updatedAt: string;
  createdAt: string;
  userPhone: string;
  userLocation: string;
  userCity: string
  msg: string;
  serviceManId: string;
  userId: string;
  _id: string;
}

export interface EnquiriesByUserResponse {
  message: string;
  result?: EnquiriesByUserResult[];
}
export interface EnquiriesByUserResult {
  __v : string;
  updatedAt: string;
  createdAt: string;
  userPhone: string;
  userLocation: string;
  userCity: string
  msg: string;
  serviceManId: string;
  userId: string;
  _id: string;
  SMan: ServiceProvider;
  //below are custom fields to show comments only - local fields
  comments: CommentResult[];
  commentToAdd: string;
  commentPostResponse: string;
  expandComments: boolean;
}

export interface CommentPost {
  senderId: number;
  enquiryId: string;
  senderUserName: string;
  senderPhone:string;
  senderFirstName:string;
  isConsumer:boolean;
  msg:string;
  parentResponseId:string;
}

export interface CommentCreateResponse {
  message: string;
  result: CommentResult;
}
export interface CommentsGetResponse {
  message: string;
  result: CommentResult[];
}

export interface CommentResult {
  _v:number;
  updatedAt:string;
  createdAt:string;
  isConsumer:boolean;
  parentResponeId:string;
  msg:string;
  senderFirstName:string;
  senderPhone:string;
  senderUserName: string;
  senderId: string;
  enquiryId: string;
  _id: string;
}



export interface Register {
  userName: string;
  phone: string;
  password: string;
  serviceName : string;
  city: string;
  location:string;
}

export interface ResetPassword {
  oldPassword: string;
  newPassword: string;
}

export interface RegisterMsg {
  _id?: string;
  message: string;
  userName?: string;
}

export interface Profile {
  firstName: string;
  lastName: string;
  state: string;
  city: string;
  location: string;
  pin: string;
  email: string;
}

export interface InitVerification {
  phone: string;
  userRole: string;
}

export interface InitVerificationResponse {
  message: string;
}

export interface CompleteVerification {
  phone: string;
  userRole: string;
}

export interface CompleteVerificationResponse {
  id:string;
  phone:string;
  message: string;
}


export interface ResetPasswordMsg {
  _id?: string;
  firstName: string;
  lastName: string;
  state: string;
  city: string;
  location: string;
  pin: string;
  email: string;
  message: string;
}

export interface ForgetPassword {
  _id?: string;
  phone: string;
}

export interface JobPost {
  userId: number;
  city: string;
  location: string;
  phone:string;
  title:string;
  serviceName:string;
  date:string;
  time:string;
  status: string
}

export interface Job {
  _v:number;
  updatedAt:string;
  createdAt:string;
  userName:string;
  userFirstName:string;
  status:string;

  serviceTime:string;
  serviceDate:string;
  description:string;
  serviceName:string;
  title:string;
  userPhone:string;
  userLocation:string;
  userCity:string;
  userId:string;
  _id:string;

  //
  //below are custom fields to show comments only - local fields
  comments: JobCommentResult[];
  commentToAdd: string;
  commentPostResponse: string;
  expandComments: boolean;

}

export interface JobCreateResponse {
  message: string;
  result: Job;
}

export interface JobsGetResponse {
  message: string;
  result: Job[];
}



export interface JobCommentPost {
  senderId: number;
  jobId: string;
  senderUserName: string;
  senderPhone:string;
  senderFirstName:string;
  isConsumer:boolean;
  msg:string;
  parentResponseId:string;
}

export interface JobCommentCreateResponse {
  message: string;
  result: JobCommentResult;
}
export interface JobCommentsGetResponse {
  message: string;
  result: JobCommentResult[];
}

export interface JobCommentResult {
  _v:number;
  updatedAt:string;
  createdAt:string;
  isConsumer:boolean;
  parentResponeId:string;
  msg:string;
  senderFirstName:string;
  senderPhone:string;
  senderUserName: string;
  senderId: string;
  jobId: string;
  _id: string;
}
