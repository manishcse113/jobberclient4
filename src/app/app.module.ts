import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ConsumersComponent } from './consumers/consumers.component';
import { ServiceProviderComponent } from './service-provider/service-provider.component';
import { FormsModule } from '@angular/forms';
import { ConsumerDetailComponent } from './consumer-detail/consumer-detail.component';
import { ServicesComponent } from './services/services.component'; // <-- NgModel lives here

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import { NgGridModule } from 'angular2-grid/modules/NgGrid.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { PageServiceService } from './services/page-service.service';
import {ServiceProviderService } from './services/serviceProvider.service';
import { HttpModule} from '@angular/http';
import {DropdownModule} from "ngx-dropdown";

//For Google Map
import { AgmCoreModule } from '@agm/core';
import { SpMapComponent } from './sp-map/sp-map.component';
//import { LoginComponent } from './login/login.component';
//import { RegisterComponent } from './register/register.component';
//import { HomeComponent } from './home/home.component';

//import {PageService} from '../services/PageService'
//import {PaginationDirective} from '../directives/pagination.directive';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// used to create fake backend
import { fakeBackendProvider } from './_helpers';

//import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserJobsComponent } from './user-jobs/user-jobs.component';
import { UserNewJobComponent } from './user-new-job/user-new-job.component';
import { UserJobDetailsComponent } from './user-job-details/user-job-details.component';
import { UserJobsContainerComponent } from './user-jobs-container/user-jobs-container.component';

//import {MyDatePickerModule} from 'mydatepicker';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { UserJobResponsesComponent } from './user-job-responses/user-job-responses.component';

import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { LoggedInComponent } from './loggedIn/loggedIn.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ConsumerProfileComponent } from './login/consumer-profile/consumer-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {AddBannerComponent} from './add-banner/add-banner.component';

import { Consumers } from './consumers/mock-consumers';

import { BrowserXhr } from '@angular/http';
import {CustExtBrowserXhr} from './cust-ext-browser-xhr';
import { sharedService } from './_services/sharedService';

import {User} from './_models/user';
//import { provide } from '@angular/core';

//import { InfiniteScrollModule } from 'angular2-infinite-scroll';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AdsenseModule } from 'ng2-adsense';

import { APP_BASE_HREF, Location } from '@angular/common';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CarriersComponent } from './carriers/carriers.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PartnersComponent } from './partners/partners.component';

import {FilterPipe, SortByPipe, JobFilterPipeLocation, JobFilterPipeStatus, SortByPipeCreateDate} from './_services/pipes'

const materialModules = [
  MatButtonModule,
  //MatMenuModule,
  //MatToolbarModule,
  //MatIconModule,
  MatCardModule,
  //MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  //MatTooltipModule
];
@NgModule({
   declarations: [
      AppComponent,
      ConsumersComponent,
      ServiceProviderComponent,
      ConsumerDetailComponent,
      ServicesComponent,
      SpMapComponent,
      AlertComponent,
      LoginComponent,
      RegisterComponent,
      HomeComponent,
      UserJobsComponent,
      UserNewJobComponent,
      UserJobDetailsComponent,
      UserJobsContainerComponent,
      DatePickerComponent,
      UserJobResponsesComponent,
      ForgetPasswordComponent,
      LoggedInComponent,
      DashboardComponent,
      ConsumerProfileComponent,
      ResetPasswordComponent,
      AddBannerComponent,
      ContactUsComponent,
      CarriersComponent,
      AboutUsComponent,
      PartnersComponent,
      FilterPipe, SortByPipe, JobFilterPipeLocation, JobFilterPipeStatus, SortByPipeCreateDate
   ],
   imports: [
      BrowserModule,
      InfiniteScrollModule,
      FormsModule,
      MatGridListModule,
      MatButtonModule,
      MatCheckboxModule,
      MatCardModule,
      NgGridModule,
      NgxPaginationModule,
      HttpModule,
      HttpClientModule,
      DropdownModule,
      AgmCoreModule.forRoot({
        apiKey: "AIzaSyDLp2HIHEh9EnqGxnf9-s_gXwrmsYTQRZ4",
      }),

     routing,
     AdsenseModule.forRoot({
      adClient: 'ca-pub-7640562161899788',
      adSlot: 7259870550,
     }),
  
     NgDatepickerModule,
               // <----- this module will be deprecated in the future version.
    MatDatepickerModule,        // <----- import(must)
    MatNativeDateModule,         // <----- import(optional)
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
   
    ReactiveFormsModule   
  ],
  providers: [ServiceProviderService,
    PageServiceService,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    //User,
    sharedService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
    },
    // provider used to create fake backend
    fakeBackendProvider,
    { provide: APP_BASE_HREF, useValue: window['_app_base'] || '/' },
    {provide: BrowserXhr, useClass:CustExtBrowserXhr}
    //,
    //{provide: LocationStrategy, useClass: HashLocationStrategy}

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

//platformBrowserDynamic().bootstrapModule(AppModule);
