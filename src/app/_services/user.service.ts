import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';


import { Observable } from 'rxjs';
import {Http, Response, Headers, RequestOptions, ResponseContentType} from '@angular/http';
import {sharedService} from './../_services/sharedService';
import { ActivatedRoute, Router } from '@angular/router';
//import { Constants } from './constants';

//import {Observable} from 'rxjs/Rx';

import {LoginMsg, Login, RegisterMsg,
        Register, domain, httpPostOptions, httpPostOptionsUpload,
        ForgetPassword, ProfileMsg, Profile, ResetPasswordMsg, ResetPassword,
         InitVerification, InitVerificationResponse, CompleteVerificationResponse, fileUpload} from '../APISymbols';

@Injectable()
export class UserService {
    constructor(private http: HttpClient, ss: sharedService, private router: Router) { 
      console.log("setting up loggedIn Status as false");
      this.loggedIn=false;
      this.ss = ss;
      this.httpClient=http;
      this.userType = "Consumer";
      this.serviceManserviceType = "";
      this.currentUser = new User();
      //httpClient: HttpClient;
      /*this.progress$ = Observable.create(observer => {
          this.progressObserver = observer
      }).share();
      */

    }
    progress:any;
    currentUser: User;
    userType:string;
    serviceManserviceType:string;
    loggedIn: boolean;// = false;
    userName: string = null;
    _id: any;
    phone: any;
    ss: sharedService;
    httpClient: HttpClient;

    getAll() { return this.http.get<User[]>('/api/users'); }

    getById(id: number) { return this.http.get('/api/users/' + id); }

    setId(id: any) { 
      this._id = id;
      console.log(this._id);
    }

    setLoggedInStatus(status: boolean) {
      this.loggedIn=status;
      console.log("loggedIn Status " + this.loggedIn);
    }

    getId(){
      let temp = this._id;
      this.clearId();
      return temp;
    }

    clearId(){ this._id = undefined; }
    //Seems Obsolete
    create(user: User) { return this.http.post('/api/users', user); }
    //Seems Obsolete
    update(user: User) { return this.http.put('/api/users/' + user.id, user); }
    //Seems Obsolete
    delete(id: number) { return this.http.delete('/api/users/' + id); }

    setCurrentUser(user: User) {
        this.currentUser = user;
        this.currentUser.userType = this.userType;
        this.userName = user.fullName;

    }

    setUserType(userType:string)
    {
      this.userType = userType;
      if (this.currentUser==null)
      {
        this.currentUser= new User();
      }
      this.currentUser.userType = this.userType;
      
      console.log("Setting User Type " + this.userType + " " + userType);
    }
    setServiceManServiceType(serviceType:string)
    {
      this.serviceManserviceType = serviceType;
    }
    getCurrentUser() {  return this.currentUser;}

    getOnlyUserFullName() { return this.userName;}

    persistCurrentUser(user: User) {
      this.currentUser = user;
      this.userName = user.fullName;
      this.currentUser.userType = this.userType;
      localStorage.setItem('currentUser', JSON.stringify(user));
  }

    removeCurrentUserFromLocalStorage() {
      console.log("Removing all records of user");
      localStorage.removeItem('currentUser');
      this.currentUser = null;
      this.currentUser = new User();
    }

    getCurrentUserFromLocalStorage() {
      if (localStorage.getItem('currentUser')) {
       //this.currentUser = localStorage.getItem('MyApp_Auth');
       this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
       this.userType = this.currentUser.userType.toString();
       this.userName  = this.currentUser.fullName 
       return this.currentUser;
      }
      else
      {
        return null;
      }
    }

    getLoggedInflag() {
      if (this.loggedIn==false) {
        console.log("user is not logged in");
        this.currentUser = this.getCurrentUserFromLocalStorage();
        if (this.currentUser!=null) {
          console.log("user exists in localstorage");
          this.userType = this.currentUser.userType.toString();
          //iniitiate auto-login - as is being done in registration case::
          this.initiateAutoLogin(this.currentUser.phone, this.currentUser.password);
        } 
      }
      return this.loggedIn;
    }

    getLogin(phone: string, pwd: string): Observable<LoginMsg> {
      this.phone = phone;
      return this.http.get<LoginMsg>(domain + this.currentUser.userType + "/" + phone+ "/" + pwd);
    }

    initiateAutoLogin(phone, password) {
      console.log("AutoLogin initiating - logging in");
      this.getLogin(phone, password)
        .subscribe(
         d => { console.log("AutoLogin success");
                this.validateResponse(d); 
                //this.invalidSubmission = false;
                this.registrationLoginCompleted();
              },
          e =>{ if(e.status == 404)  {
                  console.log("AutoLogin failure");
                  //this.invalidSubmission = true;
                }
              }
          );
    }

    //Seems Obsolete Function
    login(userName: string, id: string): Observable<Login> {
      this.loggedIn = true;
      this.userName = userName;
      this._id = id;
      console.log('hey im id' + this._id);
      return this.http.get<Login>(domain + "Consumer/");
    }

    registerUser(name: string, phone: string, pwd: string, city: string, location:string): Observable<RegisterMsg> {
      let user: Register = {userName: name, phone: phone, password: pwd, serviceName:this.serviceManserviceType, city:city, location:location};
       return this.http.post<RegisterMsg>(domain + this.currentUser.userType + "/", user, httpPostOptions);
    }

    resetPassword(pwd: string, pwd2: string): Observable<ResetPasswordMsg> {
      let rp: ResetPassword = {oldPassword: pwd, newPassword: pwd2};
       return this.http.post<ResetPasswordMsg>(domain +  this.currentUser.userType + "Reset/" + this.phone , rp, httpPostOptions);
    }

    logout(): void {
        console.log("Logout - Visit Again!" + this.userName + this._id);
        //this.loggedIn = false;
        this.userName = null;
        //this.userType = "Consumer";
        localStorage.removeItem('this.username');
        this.setLoggedInStatus(false);
        this.removeCurrentUserFromLocalStorage();
    }

    forgetPassword(phone: string): Observable<ForgetPassword> {
        return this.http.get<ForgetPassword>(domain+"ForgetBy" + this.currentUser.userType + "Phone/"+phone);
    }
    
    forgetPasswordStatus(): void { console.log('OTP Sent!');  }

    profile(state: string, city: string, location: string, pin: string, email: string, 
            firstName: string, lastName: string): Observable<ProfileMsg> 
    {
     let user_p: Profile = {state: state, city: city, location: location, pin: pin,
                                   email: email, firstName: firstName, lastName: lastName};
      return this.http.post<ProfileMsg> (domain+"update" + this.currentUser.userType + "Profile/"+this._id, user_p, httpPostOptions);
    }

    initVerification(phone: string, userRole: string): Observable<InitVerificationResponse> 
    {
       let verification_param: InitVerification = {phone: phone, userRole: userRole};
       return this.http.post<InitVerificationResponse> (domain+"Validate/", verification_param, httpPostOptions);
    }

    otpVerification(phone: string, userRole: string, otp: string): Observable<CompleteVerificationResponse> 
    {
       //let verification_param: InitVerification = {phone: phone, role: userRole};
       return this.http.get<CompleteVerificationResponse> (domain+"Validate/" + phone +"/" + userRole +"/" + otp);//, httpPostOptions);
    }
    
    registrationLoginCompleted(){
      this.ss.registrationLoginCompleted()
    }

    validateResponse(data: LoginMsg) {
      console.log(data.message);
      console.log(data.result[0].userName);
      console.log(data.result[0]._id);
      this.setId(data.result[0]._id);
      this.setLoggedInStatus(true);
      this.userName = data.result[0].userName;
      //this.currentUser.city = data.result[0].city;
      //this.currentUser.city = data.result[0].city;


      this.currentUser.id = data.result[0]._id;
      this.currentUser.updatedAt= data.result[0].updatedAt;
      this.currentUser.createdAt= data.result[0].createdAt;
      this.currentUser.photourl= data.result[0].photoUrl;
      this.currentUser.isVerified= data.result[0].isVerified;
      this.currentUser.lastName= data.result[0].lastName;
      this.currentUser.firstName= data.result[0].firstName;
      this.currentUser.pin= data.result[0].pin;
      this.currentUser.location= data.result[0].location;
      this.currentUser.city= data.result[0].city;
      this.currentUser.state= data.result[0].state;
      this.currentUser.email= data.result[0].email;
      this.currentUser.phone= data.result[0].phone;
      this.currentUser.userName= data.result[0].userName;
      this.currentUser.__v= data.result[0].__v;
      if (data.result[0].serviceName!=null)
      {
        this.currentUser.serviceType = data.result[0].serviceName;
      }
      console.log("photoUrl is " + this.currentUser.photourl);
    }

    /*

    onUpload(formData: any)
    {
      var uId=this.currentUser.id;
      var Upload = domain + "file/" + uId + "/profile/" + uId;
          //var authData = JSON.parse(this.localStorage.localStorageGet('token'));
      //var token = 'Bearer ' + authData.token;
      var self = this;
      var headers = new Headers();
      //headers.append('Content-Type', 'multipart/form-data');
      headers.set('Accept', 'application/json');
      httpPostOptions.headers.set('Accept', 'application/json');
      //headers.append('Authorization', token);
      return this.http.post<fileUpload> (Upload, formData, httpPostOptions);
      //return this.http.post(Upload , formData, { headers: headers, method: POST }).map((res: Response) => res.json());
    }
    */


    responseData: any;
    postWithFile ( context : String, subPath: String, uId : String, postData: any, files: File) {
          let url: string;
          let headers = new Headers();
          let formData:FormData = new FormData();
          formData.append('image', files, files.name);
          url = domain + "file/" + context + subPath + uId;
          if(postData !=="" && postData !== undefined && postData !==null){
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
          }
          var returnReponse = new Promise((resolve, reject) => {
             this.http.post(url, formData, httpPostOptionsUpload
            ).subscribe(
                res => {
                  console.log("upload success");
                },
                error => {
                  console.log("upload failure");
                }
            );
          });
          return returnReponse;
        }

    getFile ( context : String, subPath: String, uId : String, fileName: String): Observable<Blob>
    {
      let files: File;
      let url: string;
      let headers = new Headers();
      let formData:FormData = new FormData();
      let responseType:ResponseContentType.Blob;

      //formData.append('image', files, files.name);
      url = domain + "file/" + fileName + "/" + context + subPath + uId;
      return this.httpClient.get(url, { responseType: 'blob' })
      //.map((res: Response) => res.blob());
    }

   
    /*
    makeFileRequest (params: string[], files: File): Observable<any> {
      var uId=this.currentUser.id;
      let url: string;
      url = domain + "file/" + uId + "/profile/" + uId;
      return Observable.create(observer => {
          let formData: FormData = new FormData(),
              xhr: XMLHttpRequest = new XMLHttpRequest();

          // for (let i = 0; i < files.length; i++) {
              formData.append("uploads[]", files, files.name);
          //}

          xhr.onreadystatechange = () => {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      console.log("File upload success");
                      observer.next(JSON.parse(xhr.response));
                      observer.complete();
                  } else {
                      observer.error(xhr.response);
                      console.log("File upload serror");
                  }
              }
          };

          xhr.upload.onprogress = (event) => {
              this.progress = Math.round(event.loaded / event.total * 100);
              console.log("File upload progress " +  this.progress);

              //this.progressObserver.next(this.progress);
          };
          console.log("File upload post " );

          xhr.open('POST', url, true);
          xhr.send(formData);
      });
    }
    */
}
