import {Component, Injectable,Input,Output,EventEmitter} from '@angular/core'


@Injectable()
export class sharedService {
  @Output() loggedIn:EventEmitter<any>=new EventEmitter();
  @Output() logoutEvent:EventEmitter<any>=new EventEmitter();
  
 // @Output dataChangeObserver: EventEmitter=new EventEmitter();

   
   constructor(){
     console.log('shared service started');
   }
 
   registrationLoginCompleted()
   {
     console.log('registrationLoginCompleted Event Fired'); 
     this.loggedIn.emit(true);
   }

   logoutCompleted()
   {
     console.log('logout Event Fired'); 
     this.loggedIn.emit(false);
   }
   
   getEmittedValue()
   {
     return this.loggedIn;
   }
   
   setData(data:any) {
   // this.data = data;
    //this.dataChangeObserver.emit(this.data); 
    //return this.dataChangeObserver;
  } 
 
} 
