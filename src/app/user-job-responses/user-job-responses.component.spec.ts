import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserJobResponsesComponent } from './user-job-responses.component';

describe('UserJobResponsesComponent', () => {
  let component: UserJobResponsesComponent;
  let fixture: ComponentFixture<UserJobResponsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserJobResponsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserJobResponsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
