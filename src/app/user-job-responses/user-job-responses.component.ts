//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Input } from '@angular/core';
import {Job} from '../_models/job';
import {JobResponse} from '../_models/jobResponse';
import {MockJobResponses} from '../_models/mock_jobResponses';

import * as _ from 'underscore';
import { PageServiceService } from '../services/page-service.service';

@Component({
  selector: 'app-user-job-responses',
  templateUrl: './user-job-responses.component.html',
  styleUrls: ['./user-job-responses.component.css']
})
export class UserJobResponsesComponent implements OnInit {

  myResponses = MockJobResponses;

  constructor(
    private pagerService: PageServiceService
  ) 
  { 
    this.setPage(1);
  }

  ngOnInit() {
    this.setPage(1);
  }
  @Input() selectedJob: Job;


  // pager object
  pager: any = {};    
  // paged items
  pagedItems: any[];
  setPage(page: number) 
  {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.myResponses.length, page);
    // get current page of items
    this.pagedItems = this.myResponses.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  selectedResponse: JobResponse;
  onSelect(jobResponse: JobResponse): void {
    this.selectedResponse = jobResponse;
  }

}
