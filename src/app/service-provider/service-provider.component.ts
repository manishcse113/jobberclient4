import { Component, OnInit, Input } from '@angular/core';
import {ServiceProvider} from './ServiceProvider';
import {ServiceProviders} from './mock-serviceProviders';
import {MyFavouriteServiceProviders} from './mock-serviceProviders'
import {MyResponsesServiceProviders} from './mock-serviceProviders'
import {Service} from '../services/service'
import {Services} from '../services/mock-services'
//import {PaginationDirective} from '../directive/pagination.directive';

//following are for Pagination Support
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as _ from 'underscore';
import { PageServiceService } from '../services/page-service.service';
//import { PageServiceService } from './../service/page-service';
import { ServiceProviderService} from '../services/serviceProvider.service';
//import { Observable } from 'rxjs/Rx';
import {Subject} from "rxjs/Subject";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import {DropdownModule} from "ngx-dropdown";

import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

import {SpMapComponent} from '../sp-map/sp-map.component';
import {HomeComponent} from '../home/home.component';
import {UserJobsComponent} from '../user-jobs/user-jobs.component';
import {User} from '../_models/user';

import {City} from '../_models/city';
import {Area} from '../_models/location';
import {cities } from '../_models/mock-cities';


import { UserService } from '../_services/user.service';
import { SearchAllServiceManResult, SearchServicenManResult, SearchServiceManMsg, userTypeServiceMan, userTypeConsumer, subPathTypeProfile, likingUser, EnquiryMsgResponse, EnquiriesByUserResult } from '../APISymbols';
import { forEach } from '@angular/router/src/utils/collection';

import {FilterPipe, SortByPipe} from './../_services/pipes'

//var includes = require('lodash/collections/includes');

@Component({
  selector: 'app-service-provider',
  templateUrl: './service-provider.component.html',
  styleUrls: ['./service-provider.component.css']
})

export class ServiceProviderComponent implements OnInit {

  mockedServiceProviders = ServiceProviders;
  selectedServiceProviders: ServiceProvider[] = [];

  mockedFavouriteServiceProviders = MyFavouriteServiceProviders;

  //myFavMap: SpMapComponent;

  @ViewChild(SpMapComponent) myFavMap:SpMapComponent;

  @ViewChild(HomeComponent) homeCmp:HomeComponent;

  //@ViewChild(UserJobsComponent)
  //private userJobs: UserJobsComponent;


  public currentUser : User;
  //@ViewChild(SpMapComponent) nearbyMap:SpMapComponent;
  //@ViewChild(SpMapComponent) myRespMap:SpMapComponent;

  mockedResponsesServiceProviders = MyResponsesServiceProviders;
  respnsesServiceProviders: ServiceProvider[] = [];
  
  selectedServiceProvider: ServiceProvider;

  //Following is for test integration with the server..
  public nearbyServicemen: ServiceProvider[] = [];
  public favouriteServiceProviders: ServiceProvider[] = [];
  public allServiceProviders: ServiceProvider[] = [];
  public enquiriesByUser: EnquiriesByUserResult[]=[];
  
  //service to get records from server
  //serviceProviderService : ServiceProviderService;

  //@Input() selectedService: Service;

  //currentUser: User;
  public _selectedService:Service = Services[0];
  @Input() set selectedService(service: Service) 
  {
    //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser = this.loginService.getCurrentUser();
    this._selectedService = service ;
    this.refreshPage();
    //this.userJobs.refresh();    
  }

  public cities= cities; //cities are defined in mock-cities.ts
  public selectedCityName: string = this.cities[0].name;
  public selectedCity2: City = this.cities[0];  
  public onCitySelected() {
      this.selectedCity2 = this.cities.find(city => city.name === this.selectedCityName);
      this.selectedLocationName = this.selectedCity2.areas[0].name;
      this.onLocationSelected();
      console.log(this.selectedCity2.name);
    
      //this.userJobs.refresh();
  }

  public selectedArea :Area = this.cities[0].areas[0];
  public selectedLocationName:string = this.cities[0].areas[0].name;
  public locationFilter:string= "";//this.cities[0].areas[0].name.toString();
  public onLocationSelected() {
    this.selectedArea = this.selectedCity2.areas.find(area=>area.name===this.selectedLocationName);
    this.refreshPage();
    if (this.selectedArea.name === "All") {
      this.locationFilter = "";
    }
    else{
      this.locationFilter = this.selectedArea.name;
    }
    //this.userJobs.refresh();    
  }

  refreshPage()
  {
     //display nearby service providers
    //this.nearbyMap.clearAllMarkers();
    this.currentUser = this.loginService.currentUser;
    //this.myFavMap.clearAllMarkers();
    this.loadNearByServiceMen(this.selectedCity2.name, this._selectedService.name); 
    //this.loadMyFavouriteServiceMen();

    //this.myFavMap.addMarkersToMap(this.nearbyServicemen, 0); 
    //this.nearbyMap.setPositionForFirstMarker();

    //Following shall be executed only - if valid user has logged in:
    //this.currentUser = 
    //console.log(this.homeCmp.currentUser.firstName);
    let markerIndex : number= this.nearbyServicemen.length;

    
    if (this.currentUser!=null)
    {
      let j=0;
      //display myResponses service providers
      j=0;
      this.respnsesServiceProviders = [];
      //this.myRespMap.clearAllMarkers();
      for(let i=0; i< this.mockedResponsesServiceProviders.length; i++)
      {
          if ((this.mockedResponsesServiceProviders[i].serviceName==this._selectedService.name)
            && (this.selectedCity2.name === this.mockedServiceProviders[i].city)
          )
          {
            this.respnsesServiceProviders[j]= (this.mockedResponsesServiceProviders[i]);
            this.myFavMap.addMarker(this.respnsesServiceProviders[j],markerIndex,2);
            j=j+1;
            markerIndex++;
          }
      }
      this.setMyRespPage(1);
    }
    //this.myFavMap.setPositionForFirstMarker(); to uncomment
  }
 
  onSelect(serviceProvider: ServiceProvider): void {
    this.selectedServiceProvider = serviceProvider;
  }
 
  constructor(private pagerService: PageServiceService,
              private serviceProviderService: ServiceProviderService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private loginService: UserService                
              ) {
    this.loginService = loginService;
    this.serviceProviderService = serviceProviderService; 
    this.loadNearByServiceMen( this.cities[0].name  , this._selectedService.name ); 
    this.loadAllServiceMen(); 
    this.currentUser = this.loginService.currentUser;
  }


  //Input string 
  enquiryMsg = 'I need services';

  loadNearByServiceMen( city: String, serviceName: String ) {
      this.serviceProviderService.getNearByServiceMan(city, serviceName)
        .subscribe( searchResultSmenMsg => {
            var result = searchResultSmenMsg.message;
            this.nearbyServicemen.length = 0;
            //this.nearbyServicemen.
            if (searchResultSmenMsg.message=="Success") {
              let i=0;
              this.nearbyServicemen.length = searchResultSmenMsg.smen.length;
              this.nearbyServicemen = searchResultSmenMsg.smen;
              console.log ("Received Servicemen" + this.nearbyServicemen.length);
              this.setLikePreferences(this.nearbyServicemen);
              //this.setNearbyPage(1);
              this.onScrollNearby();
            }
            else
            {
              this.onScrollNearby();
            }
          }, err => {
               console.log("Error" + err); 
          }
        );
  }

  loadMyFavouriteServiceMen( ) {
    console.log("##########################################################");
    this.serviceProviderService.getFavouriteServiceMan()
      .subscribe( favouriteServiceManResult => {
          var result = favouriteServiceManResult.message;
          console.log (result);
          if (favouriteServiceManResult.message=="Success") {
            let i=0;
            this.favouriteServiceProviders.length  = favouriteServiceManResult.smen.length;
            this.favouriteServiceProviders  = favouriteServiceManResult.smen; //array of sman
          
            console.log(this.favouriteServiceProviders[0].userName);
            console.log("received # favourites"+ this.favouriteServiceProviders.length);
            this.favouriteServiceProviders.forEach(entry=> {
              entry.like=1;
            });
          }
          //console.log ("Received Servicemen" + this.favouriteServiceProviders.length);
          //this.setFavPage(1);
          this.onScrollFavourite();
        },err => {
            console.log("Error" + err);
        }
      );
  }


  loadAllServiceMen( ) {
    console.log("##########################################################");
    this.serviceProviderService.getAllServiceMan()
      .subscribe( searchAllServiceManResult => {
          var result = searchAllServiceManResult.message;
          console.log (result);
          if (searchAllServiceManResult.message=="Success") {
            let i=0;
            this.allServiceProviders.length  = searchAllServiceManResult.smen.length;
            this.allServiceProviders  = searchAllServiceManResult.smen; //array of sman
          
            console.log(this.allServiceProviders[0].userName);
            console.log("received # favourites"+ this.allServiceProviders.length);
            this.allServiceProviders.forEach(entry=> {
              //entry.like=1;
            });
          }
          //console.log ("Received Servicemen" + this.favouriteServiceProviders.length);
          //this.setAllSManPage(1);
          this.onScroll();
        },err => {
            console.log("Error" + err);
        }
      );
  }

  loadEnquiriesByUser( ) {
    this.serviceProviderService.getEnquiriesByUser()
      .subscribe( enquiriesByMeResponse => {
          var result = enquiriesByMeResponse.message;
          this.enquiriesByUser.length = 0;
          if (enquiriesByMeResponse.message=="Success") {
            let i=0;
            this.enquiriesByUser.length = enquiriesByMeResponse.result.length;
            this.enquiriesByUser = enquiriesByMeResponse.result;
            console.log ("Received Enquiries" + this.enquiriesByUser.length);
          }
          this.onScrollEnquiriesByUser();
        }, err => {
             console.log("Error" + err); 
        }
      );
 }


  setLikePreferences(sps: ServiceProvider[]) {
    let currentUserId = this.currentUser.id.toString();
    sps.forEach(sp=> {
      //let sp : ServiceProvider;
      //this.loadPhoto(sp = entry);
      var obj = {userId: currentUserId};
      if (sp.likedby.filter(e => e._id === currentUserId).length > 0) {
        sp.like=1;
        console.log("setting like for " + sp.userName);
      }
      else {
        sp.like=0;
        console.log("setting Unlike for " + sp.userName);
      }
    });
  }

/*
  loadPhotos(sps : SearchServicenManResult[]) {
    sps.forEach(entry=> {
      let sp : ServiceProvider;
      this.loadPhoto(sp = entry.sman);
    });
  }
  */ 

  loadPhotos2(sps : ServiceProvider[]) {
    sps.forEach(entry=> {
      let sp : ServiceProvider;
      this.loadPhoto(sp = entry);
    });
  }
  
 // loadPhotosEnquiry(enquiriesResults : EnquiriesByUserResult[]) {
  loadPhotosEnquiry(enquiriesResults : EnquiriesByUserResult[]) {
    enquiriesResults.forEach(entry=> {
      let sp : ServiceProvider;
      this.loadPhoto(sp = entry.SMan);
      entry.expandComments=false; //initializing it here:
    });
  }

  loadPhoto(sp:ServiceProvider) {
    if (sp.photoUrl=="" || sp.photoUrl==undefined ||sp.photoUrl==null) {
      sp.photoUrl = './assets/images/myImage.png';
      sp.localPhotoUrl = sp.photoUrl;
      return;
    }
    if (sp.localPhotoUrl=="" || sp.localPhotoUrl==undefined ||sp.localPhotoUrl==null) {
      this.loginService.getFile(userTypeServiceMan, subPathTypeProfile, sp._id.toString(), sp.photoUrl)
      .subscribe(data => {
        console.log("getFile Success");
        this.createImageFromBlob(data,sp );
      }, error => {
        console.log("getFile Failure");
        console.log(error);
      });
    }
  }

  createImageFromBlob(image: Blob, sp: ServiceProvider) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
       sp.localPhotoUrl = reader.result;
      }, false);
    if (image) {
       reader.readAsDataURL(image);
    }
 }

  //Paging implementation for nearby serviceproviders
  pager: any = {};    
  pagedItems: any[];
  setPage(page: number) {
   if (page < 1 || page > this.pager.totalPages) { return; }
    this.pager = this.pagerService.getPager(this.selectedServiceProviders.length, page);
    this.pagedItems = this.selectedServiceProviders.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  private noOfItemsToShowInitially: number = 10;
  private itemsToLoad: number = 10;
  public isFullListDisplayed: boolean = false;
  
  public itemsToShow = this.allServiceProviders.slice(0, this.noOfItemsToShowInitially);
  onScroll() {
    if(this.noOfItemsToShowInitially <= this.allServiceProviders.length) {
        this.noOfItemsToShowInitially += this.itemsToLoad;
        this.itemsToShow = this.allServiceProviders.slice(0, this.noOfItemsToShowInitially);
        console.log("scrolled");
        this.loadPhotos2(this.itemsToShow);
      } else {
        this.isFullListDisplayed = true;
      }
    }

    public itemsToShowNearby = this.nearbyServicemen.slice(0, this.noOfItemsToShowInitially);
    onScrollNearby() {
      if(this.noOfItemsToShowInitially <= this.nearbyServicemen.length) {
          this.noOfItemsToShowInitially += this.itemsToLoad;
          this.itemsToShowNearby = this.nearbyServicemen.slice(0, this.noOfItemsToShowInitially);
        } else {
          this.isFullListDisplayed = true;
          this.itemsToShowNearby = this.nearbyServicemen.slice(0, this.nearbyServicemen.length);
        }
        console.log("scrolled");
        this.loadPhotos2(this.itemsToShowNearby);
    }

    public itemsToShowFavourites = this.favouriteServiceProviders.slice(0, this.noOfItemsToShowInitially);
    onScrollFavourite() {
      if(this.noOfItemsToShowInitially <= this.favouriteServiceProviders.length) {
          this.noOfItemsToShowInitially += this.itemsToLoad;
          this.itemsToShowFavourites = this.favouriteServiceProviders.slice(0, this.noOfItemsToShowInitially);
        } else {
          this.isFullListDisplayed = true;
          this.itemsToShowFavourites = this.favouriteServiceProviders.slice(0, this.favouriteServiceProviders.length);
        }
        console.log("scrolled");
        this.loadPhotos2(this.itemsToShowFavourites);
    }

    public itemsToShowjobposted = this.favouriteServiceProviders.slice(0, this.noOfItemsToShowInitially);
    
    onScrollJobsPosted() {
      if(this.noOfItemsToShowInitially <= this.favouriteServiceProviders.length) {
          this.noOfItemsToShowInitially += this.itemsToLoad;
          this.itemsToShowFavourites = this.favouriteServiceProviders.slice(0, this.noOfItemsToShowInitially);
        } else {
          this.isFullListDisplayed = true;
          this.itemsToShowFavourites = this.favouriteServiceProviders.slice(0, this.favouriteServiceProviders.length);
        }
        console.log("scrolled");
        this.loadPhotos2(this.itemsToShowFavourites);
    }

    public itemsToShowEnquiriesByUser = this.enquiriesByUser.slice(0, this.noOfItemsToShowInitially);
    onScrollEnquiriesByUser() {
      if(this.noOfItemsToShowInitially <= this.enquiriesByUser.length) {
          this.noOfItemsToShowInitially += this.itemsToLoad;
          this.itemsToShowEnquiriesByUser = this.enquiriesByUser.slice(0, this.noOfItemsToShowInitially);
        } else {
          this.isFullListDisplayed = true;
          this.itemsToShowEnquiriesByUser = this.enquiriesByUser.slice(0, this.enquiriesByUser.length);
        }
        console.log("scrolled");
        this.loadPhotosEnquiry(this.itemsToShowEnquiriesByUser);
    }

    onPostComment(enquiryResult: EnquiriesByUserResult): void {
      enquiryResult.expandComments = true;
      //Post the comment here.
      if (enquiryResult.commentToAdd==null || enquiryResult.commentToAdd==="")
      {
        enquiryResult.commentPostResponse = "Type a message first";
        return;
      }
      this.serviceProviderService.postComment(enquiryResult._id, enquiryResult.commentToAdd, enquiryResult._id)
      .subscribe( commentPostResult => {
          var result = commentPostResult.message;
          console.log (result);
          if (commentPostResult.message==="Success" || commentPostResult.message==="success") {
            //
            //this.enquiryResponseDisplay = "Comment Posted on enquiry";
            this.onExpandComments(enquiryResult);
            enquiryResult.commentToAdd = "";
            enquiryResult.commentPostResponse = "Posted Successfully"; 
          }
          else{
            enquiryResult.commentPostResponse = "Posted Failed";
            //this.enquiryResponseDisplay = "Comment could not be posted on enquiry";
          }
        },err => {
            console.log("Error" + err);
            enquiryResult.commentPostResponse = "Posted Failed";
            //this.enquiryResponseDisplay = "Some issue in posting comment on Enquiry";
        }
      );
    }

    onExpandComments(enquiryResult: EnquiriesByUserResult): void {
      enquiryResult.expandComments = true;
      //Post the comment here.
      this.serviceProviderService.getEnquiryComments(enquiryResult._id)
      .subscribe( enquiryGetCommentsResult => {
          var result = enquiryGetCommentsResult.message;
          console.log (result);
          if (enquiryGetCommentsResult.message==="Success" || enquiryGetCommentsResult.message==="success") {
            //
            enquiryResult.comments = enquiryGetCommentsResult.result;
            //this.enquiryResponseDisplay = "Comments received on enquiry";
          }
          else{
            //this.enquiryResponseDisplay = "Comments not received on enquiry";
          }
        },err => {
            console.log("Error" + err);
            //this.enquiryResponseDisplay = "Some issue in getting comment on Enquiry";
        }
      );
      
    }
    onContractComments(enquiryResult: EnquiriesByUserResult): void {
      enquiryResult.expandComments = false;
    }


  nearbyPager: any = {};    
  nearbyPagedItems: ServiceProvider[];
  setNearbyPage(page: number) {
    if (page < 1 ) { return; }
    this.nearbyPager = this.pagerService.getPager(this.nearbyServicemen.length, page);
    this.nearbyPagedItems = this.nearbyServicemen.slice(this.nearbyPager.startIndex, this.nearbyPager.endIndex + 1);
    this.loadPhotos2(this.nearbyPagedItems);
  }
 
  favPager: any = {};
  favPagedItems: ServiceProvider[]
  setFavPage(page: number)  {
    if (page < 1) { return; }
    this.favPager = this.pagerService.getPager(this.favouriteServiceProviders.length, page);
    this.favPagedItems = this.favouriteServiceProviders.slice(this.favPager.startIndex, this.favPager.endIndex + 1);
    this.loadPhotos2(this.favPagedItems);
  }

  allSManPager: any = {};
  allSManPagedItems: ServiceProvider[]
  setAllSManPage(page: number)  {
    if (page < 1) { return; }
    this.allSManPager = this.pagerService.getPager(this.allServiceProviders.length, page);
    this.allSManPagedItems = this.allServiceProviders.slice(this.allSManPager.startIndex, this.allSManPager.endIndex + 1);
    this.loadPhotos2(this.allSManPagedItems);
  }

  myRespPager: any = {};    
  myRespPagedItems: any[];
  setMyRespPage(page: number) {
    if (page < 1 )  { return; }
    this.myRespPager = this.pagerService.getPager(this.respnsesServiceProviders.length, page);
    this.myRespPagedItems = this.respnsesServiceProviders.slice(this.myRespPager.startIndex, this.myRespPager.endIndex + 1);
  }
 


  selected: boolean;
  showSelected(item: ServiceProvider) {
      if (item.like==0) { 
        item.like=1;
        item.likeCount++;
        //From here issue command for like
        this.serviceProviderService.addtoFavourite(item._id)
        .subscribe(
          addToFavouriteResponse => {
            var result = addToFavouriteResponse.message;
            console.log ("Received result" + result);
            this.refreshPage();
          }, err => {
              console.log("Error" + err);
          });  
      }
      else {
         item.like=0;
         item.likeCount--;
         //From here issue command for unlike
         this.serviceProviderService.removeFromFavourite(item._id)
         .subscribe(
           removeFromFavouriteResponse => {
             var result = removeFromFavouriteResponse.message;
             console.log ("Received result" + result);
             this.refreshPage();
           }, err => {
               console.log("Error" + err);
           });  
      }
      //From here issue command for like/unlike
  }

  public enquiryMsgResponse: EnquiryMsgResponse
  public enquiryResponseDisplay: string;
  sendEnquiry(item: ServiceProvider){
    //console.log("Enquiry sent to " + item.userName + " email " + item.email + " phone "+ item.phone)
    this.serviceProviderService.sendEnquiry(item._id, this.enquiryMsg)
    .subscribe( enquiryMsgResponse => {
        var result = enquiryMsgResponse.message;
        console.log (result);
        if (enquiryMsgResponse.message=="Success") {
          //
          this.enquiryResponseDisplay = "Message sent to service provider " + item.userName;
        }
        else{
          this.enquiryResponseDisplay = "Message could not be sent to service provider " +  + item.userName + " Msg: " + this.enquiryMsg;
        }
      },err => {
          console.log("Error" + err);
          this.enquiryResponseDisplay = "Some issue in contacting service provider " +  + item.userName + " Msg: " + this.enquiryMsg;
      }
    );
  }

  ngOnInit() {
    //set google maps defaults
    this.selectedComponent = this.nearbyString;
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (): void => {
    //handle your scroll here
    //notice the 'odd' function assignment to a class field
    //this is used to be able to remove the event listener
  };

  /*
  public latitude: number = 38;
  public longitude: number = 38;
  public searchControl: FormControl;
  public zoom: number = 3;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  ngOnInit() {
    //set google maps defaults
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    this.markers =[];

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }
*/

allServiceManString: String = "AllServiceMan";
nearbyString: String = "Nearby";
favouriteString: String = "Favourites";
mapsString: String ="Maps";
myRequests: String = "MyRequests";
postedJobs:String ="PostedJobs";

public selectedComponent: String = this.allServiceManString;

selectNearby() { this.selectedComponent = this.nearbyString; this.refreshPage(); }
selectFavourite() { this.selectedComponent = this.favouriteString; this.loadMyFavouriteServiceMen();}
selectAllServiceMan() { this.selectedComponent = this.allServiceManString; this.loadAllServiceMen();}
selectMaps() { this.selectedComponent = this.mapsString; this.refreshPage();}
selectMyRequests() {this.selectedComponent = this.myRequests; this.loadEnquiriesByUser();}
selectPostedJobs() {this.selectedComponent = this.postedJobs; }



}



