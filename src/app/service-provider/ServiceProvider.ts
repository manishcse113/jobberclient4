export class ServiceProvider 
{
    constructor(
    public __v: number,
    public _id: number,
    public serviceName: string,
    public serviceIconPath : String,
    public userName: string,
    public firstName: string,
    public location: string,
    public lat: number,
    public lng: number,
    public city:string,
    public likeCount: number,
    public distance: number,
    public phone: string,

    public lastName: String,
    public state: String,
    public pin: String,
    public email: String,
    public like: number, //set -if I like it, otherwise set to 0::
    public response: number,
    public photoUrl: string,
    public isVerified: boolean,
    public localPhotoUrl: String,
    public likedby:{ _id:String, userName :String}[]
  )
    {
      like=0;
      if (photoUrl=="" ||photoUrl==undefined||photoUrl==null){ photoUrl = './../../assets/images/myImage.png'}
      
    }

  }