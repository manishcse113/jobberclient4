//import { Item } from './item';

export class Item{
    name:string;
    value:string;
}

export const ITEMS: Item[] = [
    {
       name:'User',
       value:'Consumer'
    },
    {
        name:'Service_Provider',
        value:'ServiceMan'
    }
];