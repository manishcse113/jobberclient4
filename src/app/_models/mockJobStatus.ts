import {JobStatus} from './jobstatus';
export const JobStatusArray: JobStatus[] = [
     { 'name':'All' }, 
     { 'name':'Open'},
     { 'name':'Close'}
    ];