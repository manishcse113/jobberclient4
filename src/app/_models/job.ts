export class Job {
    jobId: number;
    userName: string;
    jobTitle: string;
    jobDescription: string;
    serviceName: string;
    cityName: string;
    locationName: string;
    address: string;
    date: string;
    jobStatus: number;
    userPhone:string;
}