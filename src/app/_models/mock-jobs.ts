import {Job} from './job';
import { DateAdapter } from '@angular/material';
export const mock_jobs: Job[] = 
[ 
    { 'jobId':1,  'userName':'Manish','jobTitle': 'AC not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Electrician', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':0, userPhone:'9810313314'  },
    { 'jobId':2,  'userName':'Harish','jobTitle': 'Washing Machine not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Electrician', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':0, userPhone:'9810313314' },
    { 'jobId':3,  'userName':'Vikas','jobTitle': 'Tap not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Plumber', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':1, userPhone:'9810313314' },
    { 'jobId':4,  'userName':'Anit','jobTitle': 'Tap is leaking', 'jobDescription': 'AC power is not coming', 'serviceName': 'Plumber', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':1, userPhone:'9810313314' },
    { 'jobId':5,  'userName':'Suresh','jobTitle': 'AC not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Electrician', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':0 , userPhone:'9810313314' },
    { 'jobId':6,  'userName':'Ramesh','jobTitle': 'Washing Machine not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Electrician', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':0, userPhone:'9810313314' },
    { 'jobId':7,  'userName':'Jagadeesh','jobTitle': 'Tap not working', 'jobDescription': 'AC power is not coming', 'serviceName': 'Plumber', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':1 , userPhone:'9810313314'},
    { 'jobId':8,  'userName':'Kamesh','jobTitle': 'Tap is leaking', 'jobDescription': 'AC power is not coming', 'serviceName': 'Plumber', 'cityName':'Gurgaon', 'locationName': 'Ardee City', 'address': 'C41A', 'date': '6/Jan/2018', 'jobStatus':1, userPhone:'9810313314' }

]