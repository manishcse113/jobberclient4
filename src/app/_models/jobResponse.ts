import { ServiceProvider } from "../service-provider/ServiceProvider";


export class JobResponse {
    responseId:number;
    jobId: number;
    userName: string;
    responseTxt:string;
    responseDate:string;
    serviceProvider: ServiceProvider;

}