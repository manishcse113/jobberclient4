import {Area} from './location'
export class City 
{
  name:string;
  areas: Area[];// = [ { 'name':'Ardee City' }, { 'name':'Sector45' } ]
  constructor(name:string, areas: Area[]) {
    this.name = name;
    this.areas = areas;
  }
}