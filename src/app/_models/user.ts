import { isValid } from "date-fns";

export class User {
    id: number;
    userName: string;
    firstName: String;
    lastName: String;
    pin: String;
    location: String;
    state: String;
    city: String;
    email: String;
    isVerified: boolean;
    photourl: String;
    localPhotoUrl: String;
    updatedAt: String;
    createdAt:String;
    password: string;
    fullName: string;
    phone: string;
    loggedIn: boolean;
    __v:number;
    userType: String;
    serviceType:String;


    constructor()
    {
        
    }
    conupdateUser (id: number, phone: string, password: string, username: string)
    {
        this.id = id;
        this.phone = phone;
        this.password = password;
        this.userName = username;   
    }

    updateUser (id: number, updatedAt: string, createdAt: string, photourl:string, localPhotourl: string,
                isVerified:boolean, lastName: string, firstName: string,
                pin: String, location: string, city:string, state:string, email: string, 
                phone: string, userName: string, __v:number) 
    {
        this.id = id;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.photourl = photourl;
        this.localPhotoUrl = localPhotourl;
        this.isVerified = isVerified;
        this.lastName = lastName;
        this.firstName = firstName;
        this.pin = pin;
        this.location = location;
        this.state = state;
        this.city = city;
        this.email = email;
        this.phone = phone;
        this.userName = userName;
        this.__v=__v;
    }
}
