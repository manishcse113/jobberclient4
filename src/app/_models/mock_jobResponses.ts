import {JobResponse} from './jobResponse';
//import { DateAdapter } from '@angular/material';
export const MockJobResponses: JobResponse[] = 
[ /*
    { responseId: 0, jobId: 1, userName: 'Harish',      responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Arvind Kejriwal', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
  },
  { responseId: 1, jobId: 1, userName: 'Harish',   responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Rahul Gandhi', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
  },
  { responseId: 2, jobId: 1, userName: 'Harish',      responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Vadra', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
   },
   { responseId: 3, jobId: 1, userName: 'Harish',   responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Lalu', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
   },

   { responseId: 5, jobId: 2, userName: 'Manish',      responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Arvind Kejriwal', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
    },
    { responseId: 6, jobId: 2, userName: 'Manish',   responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
         serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Rahul Gandhi', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
    },
    { responseId: 7, jobId: 2, userName: 'Manish',      responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Vadra', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
    },
    { responseId: 8, jobId: 2, userName: 'Manish',   responseTxt: "Pleasecontact me", responseDate: '8/1/2018',
        serviceProvider: {__v: 0, _id: 3, serviceName: 'Plumber', serviceIconPath: '../../assets/images/plumber2.png', userName: 'Lalu', location: 'Ardee City', lat:28.473, lng: 77.5352, city:'Gurgaon', likeCount: 2, distance:3, phone: '9000000011', lastName: 'Aiyar', state: 'Tailnadu', pin: '123456', mail: 'abc@gmail.com', like :0, response: 1 }
    }
    */
]