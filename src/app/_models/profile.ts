export class User {
  id: number;
  firstName: string;
  lastName: string;
  state: string;
  city: string;
  location: string;
  pin: string;
  email: string;
}
