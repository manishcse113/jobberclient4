import { Component, OnInit } from '@angular/core';

import { UserService} from './../_services/user.service';

import { sharedService} from './../_services/sharedService';
import { Item, ITEMS } from '../_models/userType';
import {Services} from './../services/mock-services';
import {Service} from './../services/service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})



export class DashboardComponent implements OnInit {

  loginService: UserService;
  userType: string
  
  radioSel:any;
  radioSelected:string;
  radioSelectedString:string;
  userTypeList: Item[] = ITEMS;
  constructor(loginService: UserService, ss: sharedService) 
  {
      this.loginService = loginService;
      this.ss = ss;
      this.getLoggedInStatus();
      this.userType = this.loginService.userType;//"Consumer";
  
      //Selecting Default Radio item here
      this.radioSelected = this.userType;
      this.getSelectedUserType();
  }

  // Get row item from array  
  getSelectedUserType(){
    this.radioSel = this.userTypeList.find(Item => Item.value === this.radioSelected);
    this.radioSelectedString = JSON.stringify(this.radioSel);
    console.log("selected is " + this.radioSelectedString);
    this.userType = this.radioSel.value;
    this.radioSelected = this.userType;
    this.loginService.setUserType(this.userType);
    this.loginService.setServiceManServiceType(this.selectedService.name.toString())
    //this.selectedService.name = 

  }
  // Radio Change Event
  onItemChange(item){
    this.getSelectedUserType();
    
  }

  private services= Services; //cities are defined in mock-cities.ts
  private selectedServiceName: string = this.services[0].name.toString();
  private selectedService: Service = this.services[0];  
  private onServiceSelected() {
      this.selectedService = this.services.find(service => service.name === this.selectedServiceName);
      console.log(this.selectedService.name);
      this.loginService.setServiceManServiceType(this.selectedService.name.toString())
      //this.refreshPage();
  }

  
 // ngOnInit() {
  //}
  ss : sharedService;

  //currentUser : User;
  ngOnInit() 
  {
    this.ss.getEmittedValue().subscribe (isLoggedIn => 
    {
      console.log("Received login update from shared service = " +isLoggedIn);
      this.loggedIn = isLoggedIn;//this.searchString;
    });
  }


  loggedIn :boolean = false;
  getLoggedInStatus()
  {
    this.loggedIn = this.loginService.getLoggedInflag();
    console.log("loggedIn status " + this.loggedIn);
  }
}
