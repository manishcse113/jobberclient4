import { Component} from '@angular/core';
import { OnInit, Input } from '@angular/core';

//import {Consumer} from '../consumers';
import { Consumer } from '../consumers/consumer';


@Component({
  selector: 'app-consumer-detail',
  templateUrl: './consumer-detail.component.html',
  styleUrls: ['./consumer-detail.component.css']
})
export class ConsumerDetailComponent implements OnInit {

  //Folowing is saying consumer is an input parameter from other component Consumer
  @Input() consumer: Consumer;

  constructor() { }

  ngOnInit() {
  }

}
