import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserJobsContainerComponent } from './user-jobs-container.component';

describe('UserJobsContainerComponent', () => {
  let component: UserJobsContainerComponent;
  let fixture: ComponentFixture<UserJobsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserJobsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserJobsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
