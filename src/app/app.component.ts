import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { AlertService } from './_services/index';
import { domain } from './APISymbols';
import {sharedService} from './_services/sharedService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Jobber';

  data: any = {};

    url: string =domain + 'Consumer/';

  ss : sharedService;
  //alertService: AlertService;
  constructor(private http: Http, alertService: AlertService, ss : sharedService){
    this.getData();
    this.getUser();
    //this.alertService =  alertService;
    this.ss = ss;
    
  }

  isLoggedIn:Boolean =false;
  ngOnInit() {
    this.ss.getEmittedValue().subscribe (isLoggedIn => 
    {
      if (isLoggedIn==true)
      {
        this.isLoggedIn = true;
        console.log("Received autologin completed shared - moving to search component by default");
        this.selectedComponent = this.searchString;
      }
    });
  }

/*
    this.alertService.autoLoginCompleted.subscribe(() => {
      console.log("Received autologin completed alert - moving to search component by default");
      this.selectedComponent = this.searchString;
    });
  }
  */
  
  searchString: String = "Search";
  myJobString: String = "MyJob";
  myDashboardString: String ="User";
  myHomeString: String ="Home";

  myPartnersString: String ="Partners";
  myContactUsString: String ="ContactUs";
  myAboutUsString: String ="AboutUs";
  carriersString: String ="Carriers";

  public selectedComponent: String = this.myDashboardString; 
 
  searchServiceProvider() {
      this.selectedComponent = this.searchString
  }

  meHome() {
      this.selectedComponent = this.myHomeString;
  }
  
  myJobs() {
      this.selectedComponent = this.myJobString;
  }

  userDashboard() {
    this.selectedComponent = this.myDashboardString;
  }
  
  contactUs() {
    this.selectedComponent = this.myContactUsString;
  }

  aboutUs() {
    this.selectedComponent = this.myAboutUsString;
  }

  partners() {
    this.selectedComponent = this.myPartnersString;
  }

  carriers() {
    this.selectedComponent = this.carriersString;
  }

  getData(){

    return this.http.get(this.url).map((res: Response) => res.json())

    }

    getUser(){
      this.getData().subscribe(
        data =>{
          console.log(data);
          this.data = data;
        }
      )
    }
}
