import { Component, OnInit, Input } from '@angular/core';

//import {mock_jobs} from '../_models/mock-jobs';
//import {Job} from '../_models/job';

import * as _ from 'underscore';
import { PageServiceService } from '../services/page-service.service';
import { ServiceProviderService} from '../services/serviceProvider.service';
import { JobCommentPost, Job, JobsGetResponse, EnquiryMsgResponse, EnquiriesByUserResult } from '../APISymbols';
import { forEach } from '@angular/router/src/utils/collection';

import {City} from '../_models/city';
import {cities } from '../_models/mock-cities';
import {Area} from '../_models/location';
import {Service} from '../services/service';

import {JobStatusArray} from '../_models/mockJobStatus';
import {JobStatus} from '../_models/jobstatus'
import {JobFilterPipeStatus, JobFilterPipeLocation } from './../_services/pipes';
@Component({
  selector: 'app-user-jobs',
  templateUrl: './user-jobs.component.html',
  styleUrls: ['./user-jobs.component.css']
})
export class UserJobsComponent implements OnInit {

  

  public selectedServiceName: string;// = this.providedServices[0].name;
  public selectedCityName: string;// = this.providedCities[0].name;
  public selectedLocationName: string ;//= this.selectedCity.areas[0].name;
  @Input() set selectedService (service: Service)
  {
    this.selectedServiceName = service.name.toString();
    //if ((this.selectedLocationName!=undefined) && (this.selectedCityName!=undefined) && (this.selectedServiceName!=undefined))
    {
      this.refresh();
    }
  }
  @Input() set selectedCity(city: City)
  {
    this.selectedCityName = city.name;
    if ((this.selectedLocationName!=undefined) && (this.selectedCityName!=undefined) && (this.selectedServiceName!=undefined))
    {
      this.refresh();
    }
  }
  public locationFilter:string="";
  @Input() set selectedLocation(area: Area)
  {
    this.selectedLocationName = area.name;
    if ((this.selectedLocationName!=undefined) && (this.selectedCityName!=undefined) && (this.selectedServiceName!=undefined))
    {
      this.refresh();
    }
    if (area.name === "All") {
      this.locationFilter = "";
    }
    else{
      this.locationFilter = area.name;
    }
  }

  public jobStatusArray : JobStatus[]= JobStatusArray; //cities are defined in mock-cities.ts
  public selectedJobStatus :JobStatus = this.jobStatusArray[0];
  public selectedJobStatusName:string = this.jobStatusArray[0].name;
  public jobStatusFilterString:string= "";//this.cities[0].areas[0].name.toString();
  public onJobStatusSelected() {
    this.selectedJobStatus = this.jobStatusArray.find(status=>status.name===this.selectedJobStatusName);
    //this.refreshPage();
    if (this.selectedJobStatus.name === "All") {
      this.jobStatusFilterString = "";
    }
    else{
      this.jobStatusFilterString = this.selectedJobStatus.name;
    }
    //this.userJobs.refresh();    
  }

  constructor(
      private pagerService: PageServiceService,
      private serviceProviderService: ServiceProviderService
  ) 
  { 
    this.serviceProviderService = serviceProviderService;
    
  }

  active :boolean;
  ngOnInit() {
    //this.setJobsPage(1);
    this.active=true;
    this.refresh();
  }

  refresh() {
    //if (this.active===true)
    {
      console.log("Refresh");
      this.loadJobsByUser();
      this.loadJobsByCityService();
    }
  }

  selectedJob: Job;
  onSelect(job: Job): void {
    this.selectedJob = job;
  }

  public jobsByUser: Job[]=[];
  loadJobsByUser( ) {
    console.log("loadJobsByUser()");
    this.serviceProviderService.getJobsByMe()
      .subscribe( jobsByUser => {
          var result = jobsByUser.message;
          this.jobsByUser.length = 0;
          if (jobsByUser.message=="Success") {
            let i=0;
            this.jobsByUser.length = jobsByUser.result.length;
            this.jobsByUser = jobsByUser.result;
            console.log ("Received Jobs by current user " + this.jobsByUser.length);
          }
          this.initJobsComments(this.jobsByUser);
          this.onScrollJobsByUser();
        }, err => {
             console.log("Error" + err); 
        }
      );
 }



 private noOfItemsToShowInitially: number = 10;
 private itemsToLoad: number = 10;
 public isFullListDisplayed: boolean = false;

 public itemsToShowJobsByUser = this.jobsByUser.slice(0, this.noOfItemsToShowInitially);
 onScrollJobsByUser() {
   if(this.noOfItemsToShowInitially <= this.jobsByUser.length) {
       this.noOfItemsToShowInitially += this.itemsToLoad;
       this.itemsToShowJobsByUser = this.jobsByUser.slice(0, this.noOfItemsToShowInitially);
     } else {
       this.isFullListDisplayed = true;
       this.itemsToShowJobsByUser = this.jobsByUser.slice(0, this.jobsByUser.length);
     }
     console.log("scrolled");
     
     //this.loadPhotosJobs(this.itemsToShowJobsByUser);
 }

 public jobsCityService: Job[]=[];
 loadJobsByCityService( ) {
   console.log("loadJobsByCityService");
   if (this.selectedService)
   {
      this.selectedServiceName = this.selectedService.name.toString();
      console.log(this.selectedServiceName);
   }
   if (this.selectedCity)
   {
      this.selectedCityName = this.selectedCity.name;
      console.log(this.selectedCityName);
   }
   if (this.selectedLocation)
   {
       this.selectedLocationName = this.selectedLocation.name;
       console.log(this.selectedLocationName);
   }

   //console.log(this.selectedServiceName + " ## " + this.selectedCityName + " ## " + this.selectedLocationName)
  
  this.serviceProviderService.getJobsByCityServiceName(this.selectedCityName, this.selectedServiceName)
    .subscribe( jobs => {
        var result = jobs.message;
        this.jobsCityService.length = 0;
        if (jobs.message=="Success") {
          let i=0;
          this.jobsCityService.length = jobs.result.length;
          this.jobsCityService = jobs.result;
          console.log ("Received Jobs in city with serviceName " + this.jobsCityService.length);
        }
        this.initJobsComments(this.jobsCityService);
        this.onScrollJobsCity();
      }, err => {
           console.log("Error" + err); 
      }
    );
}

public itemsToShowJobsCity = this.jobsCityService.slice(0, this.noOfItemsToShowInitially);
onScrollJobsCity() {
 if(this.noOfItemsToShowInitially <= this.jobsCityService.length) {
     this.noOfItemsToShowInitially += this.itemsToLoad;
     this.itemsToShowJobsCity = this.jobsCityService.slice(0, this.noOfItemsToShowInitially);
   } else {
     this.isFullListDisplayed = true;
     this.itemsToShowJobsCity = this.jobsCityService.slice(0, this.jobsCityService.length);
   }
   console.log("scrolled");
   
   //this.loadPhotosJobs(this.itemsToShowJobsByUser);
}


 public initJobsComments(jobs : Job[]) {
  jobs.forEach(entry=> {
    entry.expandComments=false; //initializing it here:
  });
}

onPostComment(job: Job): void {
  job.expandComments = true;
  //Post the comment here.
  if (job.commentToAdd==null || job.commentToAdd==="")
  {
    job.commentPostResponse = "Type a message first";
    return;
  }
  this.serviceProviderService.postJobComment(job._id, job.commentToAdd, job._id)
  .subscribe( commentPostResult => {
      var result = commentPostResult.message;
      console.log (result);
      if (commentPostResult.message==="Success" || commentPostResult.message==="success") {
        //
        //this.enquiryResponseDisplay = "Comment Posted on enquiry";
        this.onExpandComments(job);
        job.commentToAdd = "";
        job.commentPostResponse = "Posted Successfully"; 
      }
      else{
        job.commentPostResponse = "Posted Failed";
        //this.enquiryResponseDisplay = "Comment could not be posted on enquiry";
      }
    },err => {
        console.log("Error" + err);
        job.commentPostResponse = "Posted Failed";
        //this.enquiryResponseDisplay = "Some issue in posting comment on Enquiry";
    }
  );
}

onJobComplete(job: Job): void {

  if (job.status === "Closed") { job.status ='Open'; }
  else {job.status ='Closed'; }
  this.serviceProviderService.updateJob(job._id, job.userCity, job.userLocation, 
    job.serviceName, job.title, job.status)
    .subscribe( jobUpdateResult => {
      var message = jobUpdateResult.message;
      console.log (message);
      if (jobUpdateResult.message==="Success" || jobUpdateResult.message==='success') {
        console.log(jobUpdateResult.result);

      }
      else
      {
      }
    },err => {
        console.log("Error" + err);
    }
  );
}

 onExpandComments(job: Job): void {
  job.expandComments = true;  
  //Post the comment here.
  this.serviceProviderService.getJobComments(job._id)
  .subscribe( jobGetCommentsResult => {
      var result = jobGetCommentsResult.message;
      console.log (result);
      if (jobGetCommentsResult.message==="Success" || jobGetCommentsResult.message==="success") {
        //
        job.comments = jobGetCommentsResult.result;
      }
      else{
      }
    },err => {
        console.log("Error" + err);
    }
  );
}

onContractComments(job: Job): void {
  job.expandComments = false;
}


 /*
 loadPhotosJobs(enquiriesResults : EnquiriesByUserResult[]) {
  enquiriesResults.forEach(entry=> {
    let sp : ServiceProvider;
    this.loadPhoto(sp = entry.SMan);
    entry.expandComments=false; //initializing it here:
  });
}

loadPhoto(sp:ServiceProvider) {
  if (sp.photoUrl=="" || sp.photoUrl==undefined ||sp.photoUrl==null) {
    sp.photoUrl = './../../assets/images/myImage.png';
    sp.localPhotoUrl = sp.photoUrl;
    return;
  }
  if (sp.localPhotoUrl=="" || sp.localPhotoUrl==undefined ||sp.localPhotoUrl==null) {
    this.loginService.getFile(userTypeServiceMan, subPathTypeProfile, sp._id.toString(), sp.photoUrl)
    .subscribe(data => {
      console.log("getFile Success");
      this.createImageFromBlob(data,sp );
    }, error => {
      console.log("getFile Failure");
      console.log(error);
    });
  }
}

createImageFromBlob(image: Blob, sp: ServiceProvider) {
  let reader = new FileReader();
  reader.addEventListener("load", () => {
     sp.localPhotoUrl = reader.result;
    }, false);
  if (image) {
     reader.readAsDataURL(image);
  }
}
*/

}
