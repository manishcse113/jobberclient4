import { Component, OnInit, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService, AuthenticationService } from '../_services/index';


import { LoginMsg, Login } from '../APISymbols';
import { UserService } from '../_services/user.service';
import { EventEmitter } from 'events';
import {sharedService} from './../_services/sharedService';

import { User } from '../_models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']

})


export class LoginComponent implements OnInit {

  constructor( private loginService: UserService,

    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    ss:sharedService
  ) 
  { 
    this.ss=ss;
    this.currentUser = new User();
  }

  loginFormGroup: FormGroup;
  invalidSubmission: boolean = false;
  loading = false;
  returnUrl: string;
  _id: string;
  user: Login;
  data: LoginMsg;
  ss:sharedService;
  currentUser: User;

  ngOnInit() {

    // reset login status
    console.log("Login Init doing - logout");
    //this.loginService.logout();

    this.loginFormGroup = new FormGroup({
      'phoneCtr': new FormControl("", [Validators.pattern('^[0-9]+$')]),
      'pwdCtr': new FormControl(""),
      'rememberCtr' : new FormControl("")
    });
    

  }

  errorMessage : String;

  get phoneCtr() { return this.loginFormGroup.get('phoneCtr'); }

  get rememberCtr() { return this.loginFormGroup.get('rememberCtr'); }

  onSubmit() {
    this.loading = true;
    let phoneCtr = this.loginFormGroup.get('phoneCtr');
    let rememberFlag = this.loginFormGroup.get('rememberCtr');
    if (!(phoneCtr.hasError("required") || phoneCtr.hasError("pattern"))) 
    {
        let pwdCtr = this.loginFormGroup.get('pwdCtr');
        
        console.log("Register done -logging in");
        this.currentUser.phone=phoneCtr.value;
        this.currentUser.password=pwdCtr.value;
        this.currentUser.userType = this.loginService.userType;
        this.loginService.setCurrentUser(this.currentUser);
        this.loginService.getLogin(phoneCtr.value, pwdCtr.value)
          .subscribe(
            d => {
                  this.validateResponse(d); 
                  this.invalidSubmission = false;
                  this.registrationLoginCompleted();
                  this.data = d;
                  console.log(this.data);
                  console.log(d.result[0]._id);
                  //this.currentUser = new User(parseInt(d.result[0]._id), phoneCtr.value, pwdCtr.value, "");
                  
                  if (rememberFlag.value)
                  {
                    console.log("Remember the user");
                    this.loginService.persistCurrentUser(this.currentUser);
                  }
                  else
                  {
                    console.log("Don't Remember the user");
                    this.loginService.removeCurrentUserFromLocalStorage();
                    this.loginService.setCurrentUser(this.currentUser)
                  }
                },
            e =>{ 
                  this.invalidSubmission = true;
                  if(e.status == 404) 
                  {
                    this.errorMessage = "Invalid Credentials" 
                  }
                  else
                  {
                    this.errorMessage = "Invalid Credentials"
                  }
                  
                }
            );
    }
    else
    {
      this.invalidSubmission = true;
      this.errorMessage = "Telephone is 10 digit or 12 digit including country code";
    }
  }

  registrationLoginCompleted(){
    this.ss.registrationLoginCompleted()
  }
  validateResponse(data: LoginMsg) {
    console.log(data.message);
    console.log(data.result[0].userName);
    console.log(data.result[0]._id);
    this.loginService.setId(data.result[0]._id);
    this.loginService.setLoggedInStatus(true);

    this.currentUser.id = data.result[0]._id;
    this.currentUser.updatedAt= data.result[0].updatedAt;
    this.currentUser.createdAt= data.result[0].createdAt;
    this.currentUser.photourl= data.result[0].photoUrl;
    this.currentUser.isVerified= data.result[0].isVerified;
    this.currentUser.lastName= data.result[0].lastName;
    this.currentUser.firstName= data.result[0].firstName;
    this.currentUser.pin= data.result[0].pin;
    this.currentUser.location= data.result[0].location;
    this.currentUser.city= data.result[0].city;
    this.currentUser.state= data.result[0].state;
    this.currentUser.email= data.result[0].email;
    this.currentUser.phone= data.result[0].phone;
    this.currentUser.userName= data.result[0].userName;
    this.currentUser.__v= data.result[0].__v;
    if (data.result[0].serviceName!=null)
    {
      this.currentUser.serviceType = data.result[0].serviceName;
    }

  }

}
