import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../_services';
import { Router } from '@angular/router';
import { Form, FormGroup, AbstractControl, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import { ProfileMsg, domain } from '../../APISymbols';
import { LoginComponent } from '../../login/login.component';
import { User } from '../../_models';
//import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-consumer-profile',
  templateUrl: './consumer-profile.component.html',
  styleUrls: ['./consumer-profile.component.css']
})
export class ConsumerProfileComponent implements OnInit {
  constructor(private loginService: UserService, private http: HttpClient, private router: Router, private element: ElementRef) 
  {
    this.isVerified = false;
    //this.currentUser = currentUser;
    this.element = element;
    this.otpState = "ToRequest";

    this.currentUser = new User();
    this.currentUser = this.loginService.getCurrentUser();
    this.initDataInForm();

    this.otpHintMessage = "Verified Users gets more attention";
    this.userTypeConsumer="Consumer";
    this.userTypeServiceMan = "ServiceMan";
    this.subPathTypeProfile = "/profile/";
  }
  userTypeConsumer: String;
  userTypeServiceMan: String;
  subPathTypeProfile:String;

  currentUser: User;
  profileFormGroup: FormGroup;
  invalidSubmission: boolean = false;
  fnamectr: AbstractControl;
  phonectr: AbstractControl;
  lnamectr: AbstractControl;
  statectr: AbstractControl;
  emailctr: AbstractControl;
  pinctr: AbstractControl;
  locationctr: AbstractControl;
  cityctr: AbstractControl;
  email: AbstractControl;
  otpctr:AbstractControl;
  isVerified: boolean;
  serviceType: String;

  ngOnInit() {
    this.initDataInForm();
  }

  initDataInForm()
  {
    this.profileFormGroup = new FormGroup({
      'fnamectr': new FormControl("", [Validators.required]),
      'phonectr': new FormControl("", [Validators.pattern('^[0-9]+$'), Validators.required]),
      'lnamectr': new FormControl("", [Validators.required]),
      'statectr': new FormControl("", [Validators.required]),
      'cityctr': new FormControl("", [Validators.required]),
      'pinctr': new FormControl("",  [Validators.required]),
      'locationctr': new FormControl("", [Validators.required]),
      'emailctr': new FormControl("", [Validators.required]),
      'otpctr': new FormControl("", [Validators.required])
    });

    this.fnamectr = this.profileFormGroup.get('fnamectr');
    this.phonectr = this.profileFormGroup.get('phonectr');
    this.lnamectr = this.profileFormGroup.get('lnamectr');
    this.statectr = this.profileFormGroup.get('statectr');
    this.cityctr = this.profileFormGroup.get('cityctr');
    this.pinctr = this.profileFormGroup.get('pinctr');
    this.emailctr = this.profileFormGroup.get('emailctr');
    this.locationctr = this.profileFormGroup.get('locationctr');
    this.otpctr = this.profileFormGroup.get('otpctr');

    console.log("initDatainform");
    this.currentUser = this.loginService.getCurrentUser();
    console.log(this.currentUser.phone);
    this.fnamectr.setValue(this.currentUser.firstName);
    this.lnamectr.setValue(this.currentUser.lastName);
    this.phonectr.setValue(this.currentUser.phone);
    this.statectr.setValue(this.currentUser.state);
    this.cityctr.setValue(this.currentUser.city);
    this.pinctr.setValue(this.currentUser.pin);
    this.emailctr.setValue(this.currentUser.email);
    this.locationctr.setValue(this.currentUser.location);
    this.isVerified = this.currentUser.isVerified;
    this.serviceType = this.currentUser.serviceType;

    if (this.isVerified)
    {
      this.otpHintMessage ="You are verified";
    }
    this.userTypeConsumer="Consumer";
    this.userTypeServiceMan = "ServiceMan";
    this.subPathTypeProfile = "/profile/";
  
    this.getProfilePic(this.currentUser.userType);
    console.log(this.phonectr.value);
  }

  getPhto()
  {

  }

  onSubmit(){
    this.statectr = this.profileFormGroup.get('statectr');
    this.cityctr = this.profileFormGroup.get('cityctr');
    this.locationctr = this.profileFormGroup.get('locationctr');
    this.pinctr = this.profileFormGroup.get('pinctr');
    this.emailctr = this.profileFormGroup.get('emailctr');
    this.fnamectr = this.profileFormGroup.get('fnamectr');
    this.lnamectr = this.profileFormGroup.get('lnamectr');
    console.log("firstname " + this.fnamectr.value);
    this.loginService.profile(this.statectr.value,
                                   this.cityctr.value,
                                   this.locationctr.value,
                                   this.pinctr.value,
                                   this.emailctr.value,
                                   this.fnamectr.value,
                                   this.lnamectr.value).subscribe(
      d => { 
            this.invalidSubmission = false; 
            this.onSub(d);
            console.log("Profile update success" + d)
          },
      e => {
              if(e.status == 404) 
              {this.invalidSubmission = true;
                console.log("Profile update failure " + e);
              }
           });
  }

  onSub(data: ProfileMsg) {
    //if(data._id) {
      console.log(data.consumer.firstName);
      this.currentUser.id = data.consumer._id;
      this.currentUser.photourl= data.consumer.photoUrl;
      this.currentUser.isVerified= data.consumer.isVerified;
      this.currentUser.lastName= data.consumer.lastName;
      this.currentUser.firstName= data.consumer.firstName;
      this.currentUser.pin= data.consumer.pin;
      this.currentUser.location= data.consumer.location;
      this.currentUser.city= data.consumer.city;
      this.currentUser.state= data.consumer.state;
      this.currentUser.email= data.consumer.email;
      this.loginService.setCurrentUser(this.currentUser);
      this.getProfilePic(this.userTypeConsumer);


      //this.router.navigate(['..', 'loggedIn']);
    /*
    } 
    else 
    { 
      console.log(data.message);
    }
    */
  }

  activeColor: string = 'green';
  baseColor: string = '#ccc';
  overlayColor: string = 'rgba(255,255,255,0.5)';
  
  dragging: boolean = false;
  loaded: boolean = false;
  imageLoaded: boolean = false;
  imageSrc: String = '';

  handleDragEnter() {
    this.dragging = true;
  }

  handleDragLeave() {
      this.dragging = false;
  }

  handleDrop(e) {
      e.preventDefault();
      this.dragging = false;
      this.handleInputChange(e);
  }

  handleImageLoad() {
      this.imageLoaded = true;
    // this.iconColor = this.overlayColor;
  }

  file : File;
  handleInputChange(e) {
      this.file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
      var pattern = /image-*/;
      var reader = new FileReader();

      if (!this.file.type.match(pattern)) {
          alert('invalid format');
          return;
      }
      this.loaded = false;
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(this.file);
  }

  _handleReaderLoaded(e) {
    var reader = e.target;
    this.imageSrc = reader.result;
    this.loaded = true;
  }

  otpState: String;
  otpHintMessage:String;
  verifyInit()
  {
    this.otpState="Requested";
    this.otpctr.setValue("");
    this.otpHintMessage = "Initiating Verification. One time password OTP will be sent on your mobile."
    this.loginService.initVerification(this.phonectr.value, this.loginService.userType)
    .subscribe(
        d => { 
          this.invalidSubmission = false; 
          //this.onSub(d);
          console.log("Otp Sent" + d)
          this.otpHintMessage = "One time password OTP has been sent on your mobile."
          this.otpState="Requested";
        },
        e => {
          if(e.status == 404) 
          {
            this.invalidSubmission = true;
            console.log("Otp could not be sent " + e);
            this.otpHintMessage = "One time password OTP could not be sent on your mobile."
            this.otpState="ToRequest";
          }
      });
  }

  otpVerify()
  {
    this.otpState="Submitted";
    this.otpctr = this.profileFormGroup.get('otpctr');
    if (this.otpctr.value=="")
    {
      this.otpHintMessage = "One time password OTP can not be blank."
      this.otpState="ToRequest";
      return;
    }
    this.loginService.otpVerification(this.phonectr.value, this.loginService.userType, this.otpctr.value )
    .subscribe(
        d => { 
          this.otpHintMessage = "Verified."
          this.invalidSubmission = false; 
          //this.onSub(d);
          console.log("Otp verified" + d)
          this.onOtpConfirmReceived();
          this.otpState="Submitted";
        },
        e => {
          if(e.status == 404) 
          {
            this.invalidSubmission = true;
            this.otpHintMessage = "Wrong otp. Try again."
            console.log("Otp could not be verified " + e);
            this.otpState="ToRequest";
          }
      });
  }

  otpCancel()
  {
    this.otpState="ToRequest";
    this.otpHintMessage ="";
  }

  onOtpConfirmReceived()
  {
    this.isVerified = true;
    this.otpHintMessage ="You are verified";
  }  


  onUpload()
  {

    console.log("onUpload");
    //console.log(formData);
    console.log(this.file.name);
    //let postData = {image:this.file.name}//, field2:"field2"}; // Put your form data variable. This is only example.
    this.loginService.postWithFile(this.currentUser.userType, this.subPathTypeProfile, this.currentUser.id.toString(), [], this.file);
    //this.loginService.makeFileRequest([],this.file);
  }

  isImageLoading = false;
  getProfilePic(userType: String)
  {
    console.log("getProfilePic " + userType +" " + this.currentUser.photourl);
    if(this.currentUser.photourl !=="" && this.currentUser.photourl !== undefined && this.currentUser.photourl !==null)
    {
      //if(this.currentUser.localPhotoUrl !=="" && this.currentUser.localPhotoUrl !== undefined && this.currentUser.localPhotoUrl !==null)
      //{
      //always get the fresh photo - as we don't know if pic has changed in backend or not:: even if local file is present
      if (this.currentUser.userType === this.userTypeConsumer)
      {
        userType = this.userTypeConsumer;
        console.log("consumer");
      }
      else if (this.currentUser.userType === this.userTypeServiceMan)
      {
        userType = this.userTypeServiceMan;
      }
      //get the pic here:
      this.isImageLoading = true;
      this.loginService.getFile(userType, this.subPathTypeProfile, this.currentUser.id.toString(), this.currentUser.photourl)
      .subscribe(data => {
        console.log("getFile Success");
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log("getFile Failure");
        console.log(error);
      });
    }
  }

  imageToShow: any;
  createImageFromBlob(image: Blob) {
     let reader = new FileReader();
     reader.addEventListener("load", () => {
        this.currentUser.localPhotoUrl = reader.result;
        this.imageSrc = this.currentUser.localPhotoUrl;
        console.log("createImageFromBlob setImage");
     }, false);
     if (image) {
        reader.readAsDataURL(image);
     }
  }
}
