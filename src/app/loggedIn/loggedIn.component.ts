import { Component, OnInit } from '@angular/core';
import { LoginComponent} from '../login/login.component';
import { UserService } from '../_services/user.service';
import {sharedService} from './../_services/sharedService';
@Component({
  selector: 'app-loggedIn',
  templateUrl: './loggedIn.component.html',
  styleUrls: ['./loggedIn.component.css']
})

export class LoggedInComponent implements OnInit {
  userName: string;
  ss : sharedService;
  userService : UserService;

  constructor(userService: UserService, ss:sharedService) {
    this.ss = ss;
    this.userService = userService;
    this.profile = true;
  }

  ngOnInit() { 
         this.userName = this.userService.getOnlyUserFullName() ;
    }

  logout(){
    console.log("loggedIn - logout");
    this.userService.logout();
    this.ss.logoutCompleted()    
  }

  initUserName(){
    this.userName = this.userService.getOnlyUserFullName();
    console.log ("user is " + this.userName);
  }

  reset: boolean;
  initReset(){
    this.initUserName();
    console.log("loggedIn - reset");
    this.reset=true;  
    this.profileCancel();
  }

  resetCancel(){
    this.initUserName();
    console.log("loggedIn - reset cancel");
    this.reset=false;   
    //this.profileCancel();
  }

  profile: boolean;
  initProfile(){
    this.initUserName();
    console.log("loggedIn - profile Edit");
    this.profile=true;
    this.resetCancel();
  }

  profileCancel(){
    this.initUserName();
    console.log("loggedIn - profile cancel");
    this.profile=false;
    //this.resetCancel();
  }
}
