import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../_services/user.service';
import { ResetPasswordMsg } from '../APISymbols';
import { sharedService } from '../_services/sharedService';

import { User } from '../_models';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, ss:sharedService)
  {
     this.ss=ss;
  }

  ss: sharedService;

  rpFormGroup: FormGroup;
  invalidSubmission: boolean = false;
  pwdCtr1: AbstractControl;
  pwdCtr2: AbstractControl;
  pwdCtr3: AbstractControl;

  ngOnInit() {

    this.rpFormGroup = new FormGroup({
      'pwdCtr1': new FormControl("", [Validators.required]),
      'pwdCtr2': new FormControl("", [Validators.required]),
      'pwdCtr3': new FormControl("", [Validators.required])
    }, { 
      validators: this.passwordValidator
    });


    this.pwdCtr1 = this.rpFormGroup.get('pwdCtr1');
    this.pwdCtr2 = this.rpFormGroup.get('pwdCtr2');
    this.pwdCtr3 = this.rpFormGroup.get('pwdCtr3');
  }

   passwordValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
     let pwd1 = control.get('pwdCtr1').value;
     let pwd2 = control.get('pwdCtr2').value;
     let pwd3 = control.get('pwdCtr3').value;
     return pwd2 !== pwd3 ? {'pwdMismatch': true} : null;
  }

  errorMessage: String;
  onCancel() {
    this.ss.registrationLoginCompleted();
  }
  onSubmit() {
    let pwdCtr1 = this.rpFormGroup.get('pwdCtr1'); //old password
    let pwdCtr2 = this.rpFormGroup.get('pwdCtr2'); //new password
    let pwdCtr3 = this.rpFormGroup.get('pwdCtr3'); //new password repeat
    if (!(pwdCtr1.hasError("required") || pwdCtr1.hasError("pattern") || 
         pwdCtr2.hasError("required") || pwdCtr2.hasError("pattern") || 
         pwdCtr3.hasError("required") || pwdCtr3.hasError("pattern") || 
         this.rpFormGroup.hasError("pwdMismatch")) ) 
    {
        if (pwdCtr1.value.length==0 || pwdCtr2.value==0 || pwdCtr3.value.length==0)
        {
            this.invalidSubmission = true;
            this.errorMessage = "Empty Fields";
            return;
        }

        let currentUser= new User();
        currentUser.userType = this.userService.userType;
        this.userService.setCurrentUser(currentUser);

        this.userService.resetPassword(this.pwdCtr1.value,
                                    this.pwdCtr2.value)
                                    .subscribe
                                    (
        d => { 
                this.invalidSubmission = false; this.onReset(d);
                console.log(d);
                console.log('id:' + d._id);
                console.log( 'username:' + d.firstName);
                console.log('message:' + d.message);
                //Initiate logout -
                this.logout(); 

              },
        e => { 
                if(e.status == 404) 
                { 
                  this.invalidSubmission = true;
                  this.errorMessage = "Invalid Credentials";
                }
             });
    }
    else
    {
      this.invalidSubmission = true;
      this.errorMessage = "Password is min 8 char & new password repeat should be same";
    }
  }


  onReset(data: ResetPasswordMsg) {
      console.log(data);
      this.router.navigate(['..', 'login']);
  }

  logout(){
    console.log("loggedIn - logout");
    this.userService.logout();
    this.ss.logoutCompleted()    
  }

}

