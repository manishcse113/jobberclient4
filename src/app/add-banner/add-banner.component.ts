import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AdsenseModule } from 'ng2-adsense';

@Component({
  selector: 'app-add-banner',
  templateUrl: './add-banner.component.html',
  styleUrls: ['./add-banner.component.css']
})
export class AddBannerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(()=>{
     try{
       (window['adsbygoogle'] = window['adsbygoogle'] || []).push({});
     }catch(e){
       console.error("error");
     }
   },2000);
}     

}
