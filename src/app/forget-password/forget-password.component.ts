import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { ForgetPassword } from '../APISymbols';
import { UserService } from '../_services/user.service';
import { User } from '../_models';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  constructor(private router: Router, private loginService: UserService) { }

  forgetPassGroup: FormGroup;
  invalidSubmission: boolean = false;

  ngOnInit() {
    this.forgetPassGroup = new FormGroup({
      'phoneCtr': new FormControl("", [Validators.pattern('^[0-9]+$')]),

    });
  }

  get phoneCtr() { return this.forgetPassGroup.get('phoneCtr'); }

  message: String;
  errorMessage: String;
  onSubmit() {
    let phoneCtr = this.forgetPassGroup.get('phoneCtr');
    if (!(phoneCtr.hasError("required") || phoneCtr.hasError("pattern") ))
    {
      if (phoneCtr.value.length==0)
      {
          this.invalidSubmission = true;
          this.errorMessage = "Empty Fields";
          return;
      }
      let currentUser= new User();
      currentUser.userType = this.loginService.userType;
      this.loginService.setCurrentUser(currentUser);
      this.loginService.forgetPassword(phoneCtr.value).subscribe(
        d => { this.validateResponse(d); this.invalidSubmission = false;
                this.message="Password has been sent to Mobile";
            },
        e => { if(e.status == 404) this.invalidSubmission = true;
                this.message = "";
            });
    }
    else
    {
      this.invalidSubmission = true;
    }
  }

    validateResponse(data: ForgetPassword) {
        this.loginService.forgetPasswordStatus();
        //this.router.navigate(['..','results']);
    }
}
