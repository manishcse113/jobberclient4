import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';


import {  RegisterMsg } from '../APISymbols';
import { UserService } from '../_services/user.service';
import {AlertService} from '../_services/alert.service';
import { LoginMsg, Login } from '../APISymbols';
import {sharedService} from './../_services/sharedService';
import { User } from '../_models';

import {City} from '../_models/city';
import {Area} from '../_models/location';
import {cities } from '../_models/mock-cities';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private loginService: UserService, private router: Router, ss:sharedService)
   {
      this.ss=ss;
      //this.currentUser=currentUser;
      this.currentUser = new User();
   }

  ss : sharedService;
  registerFormGroup: FormGroup;
  invalidSubmission: boolean = false;
  nameCtr: AbstractControl;
  phoneCtr: AbstractControl;
  pwdCtr1: AbstractControl;
  pwdCtr2: AbstractControl;
  rememberCtr: AbstractControl;
  alertService : AlertService;
  currentUser: User;
  errorMessage : String;
  ngOnInit() {
    this.registerFormGroup = new FormGroup({
      'nameCtr': new FormControl("", [Validators.required]),
      'phoneCtr': new FormControl("", [Validators.pattern('^[0-9]+$'), Validators.required]),
      'pwdCtr1': new FormControl("", [Validators.required]),
      'pwdCtr2': new FormControl("", [Validators.required]),
      'rememberCtr' : new FormControl("")
      }, { validators: this.passwordValidator});

    this.nameCtr = this.registerFormGroup.get('nameCtr');
    this.phoneCtr = this.registerFormGroup.get('phoneCtr');
    this.pwdCtr1 = this.registerFormGroup.get('pwdCtr1');
    this.pwdCtr2 = this.registerFormGroup.get('pwdCtr2');
    this.rememberCtr = this.registerFormGroup.get('rememberCtr');
    this.alertService = new AlertService(this.router);
  }

  passwordValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    let pwd1 = control.get('pwdCtr1').value;
    let pwd2 = control.get('pwdCtr2').value;
    return pwd1 !== pwd2 ? {'pwdMismatch': true} : null;
  }

  onSubmit() {
    let phoneCtr = this.registerFormGroup.get('phoneCtr');
    let nameCtr = this.registerFormGroup.get('nameCtr');
    let pwdCtr1 = this.registerFormGroup.get('pwdCtr1');
    let rememberFlag = this.registerFormGroup.get('rememberCtr');
    
    console.log("Register form submitted");

    if ((!this.selectedCityName) || !this.selectedLocationName)
    {
      this.errorMessage = "Please select City & Location";
      return;
    }
    if (!(phoneCtr.hasError("required") || phoneCtr.hasError("pattern") ||
      pwdCtr1.hasError("required") || pwdCtr1.hasError("pattern") || this.registerFormGroup.hasError("pwdMismatch")) || nameCtr.hasError("required")) 
      {
        if (phoneCtr.value.length==0 || nameCtr.value==0 || pwdCtr1.value.length==0 || this.pwdCtr2.value.length==0)
        {
            this.invalidSubmission = true;
            this.errorMessage = "Empty Fields";
            return;
        }
        this.currentUser.phone = phoneCtr.value;
        this.currentUser.userName = nameCtr.value;
        this.currentUser.password = pwdCtr1.value;
        //this.currentUser.userType = this.loginService.
        
        this.loginService.registerUser(nameCtr.value, phoneCtr.value, this.pwdCtr2.value, this.selectedCityName, this.selectedLocationName).subscribe(
          d => { 
               this.invalidSubmission = false; 
               this.onRegistration(d);
               console.log('id:' + d._id);
               console.log( 'username:' + d.userName);
               console.log('message:' + d.message);
               //this.currentUser = new User(parseInt(d._id), phoneCtr.value, pwdCtr1.value, "");               
               if (rememberFlag.value)
               {
                 console.log("Remember the user");
                 this.loginService.persistCurrentUser(this.currentUser);
               }
               else
               {
                 console.log("Don't Remember the user");
                 this.loginService.removeCurrentUserFromLocalStorage();
                 this.loginService.setCurrentUser(this.currentUser)                 
                }

              },
        e =>  { 
                //if(e.status == 404)
                 
                this.invalidSubmission = true;
                if(e.status == 404) 
                {
                  this.errorMessage = "User Already Exists";
                }
                else
                {
                  this.errorMessage = e.message;
                }
              });
      }
      else
      {
        this.invalidSubmission = true;
        this.errorMessage = "Validate Password is min 8 char, Telephone is 12 digit including country code";
      }
  }

  onRegistration(data: RegisterMsg) {
    if(data.userName)
    {
      console.log("Register done - logging in");
      //this.currentUser.phone = phoneCtr.value;
      //this.currentUser.userName = nameCtr.value;
      //this.currentUser.password = pwdCtr1.value;
      
      this.loginService.getLogin(this.phoneCtr.value, this.pwdCtr1.value)
        .subscribe(
         d => {
                this.validateResponse(d); 
                this.invalidSubmission = false;
                this.registrationLoginCompleted();
                //this.currentUser = new User(parseInt(d.result[0]._id), this.phoneCtr.value, this.pwdCtr1.value, "");
                this.loginService.setCurrentUser(this.currentUser)
              },
          e =>{ 
                if(e.status == 404) 
                this.invalidSubmission = true;
              }
          );
    } 
    else 
    {
      console.log(data.message);
    }
  }

  initiateAutoLogin(phone, password)
  {
    console.log("AutoLogin initiating - logging in");
    this.currentUser.phone = phone;
    //this.currentUser.userName = nameCtr.value;
    this.currentUser.password = password;
    
    this.loginService.getLogin(phone, password)
      .subscribe(
       d => {
              this.validateResponse(d); 
              this.invalidSubmission = false;
              this.loginService.setCurrentUser(this.currentUser)
              this.registrationLoginCompleted();
            },
        e =>{ 
              if(e.status == 404) 
              this.invalidSubmission = true;
            }
        );
  }

  registrationLoginCompleted(){
    this.ss.registrationLoginCompleted()
  }
  validateResponse(data: LoginMsg) {
    console.log(data.message);
    console.log(data.result[0].userName);
    console.log(data.result[0]._id);
    this.loginService.setId(data.result[0]._id);
    this.loginService.setLoggedInStatus(true);
    this.currentUser.id = data.result[0]._id;
    this.currentUser.updatedAt= data.result[0].updatedAt;
    this.currentUser.createdAt= data.result[0].createdAt;
    this.currentUser.photourl= data.result[0].photoUrl;
    this.currentUser.isVerified= data.result[0].isVerified;
    this.currentUser.lastName= data.result[0].lastName;
    this.currentUser.firstName= data.result[0].firstName;
    this.currentUser.pin= data.result[0].pin;
    this.currentUser.location= data.result[0].location;
    this.currentUser.city= data.result[0].city;
    this.currentUser.state= data.result[0].state;
    this.currentUser.email= data.result[0].email;
    this.currentUser.phone= data.result[0].phone;
    this.currentUser.userName= data.result[0].userName;
    this.currentUser.__v= data.result[0].__v;
    if (data.result[0].serviceName!=null)
    {
      this.currentUser.serviceType = data.result[0].serviceName;
    }
  }

  public cities= cities; //cities are defined in mock-cities.ts
  public selectedCityName: string = this.cities[0].name;
  public selectedCity2: City = this.cities[0];  
  public onCitySelected() {
      this.selectedCity2 = this.cities.find(city => city.name === this.selectedCityName);
      this.selectedLocationName = this.selectedCity2.areas[0].name;
      this.onLocationSelected();
      console.log(this.selectedCity2.name);
    
      //this.userJobs.refresh();
  }

  public selectedArea :Area = this.cities[0].areas[0];
  public selectedLocationName:string = this.cities[0].areas[0].name;
  public locationFilter:string= "";//this.cities[0].areas[0].name.toString();
  public onLocationSelected() {
    this.selectedArea = this.selectedCity2.areas.find(area=>area.name===this.selectedLocationName);
    //this.refreshPage();
    /*
    if (this.selectedArea.name === "All") {
      this.locationFilter = "";
    }
    else{
      this.locationFilter = this.selectedArea.name;
    }
    */
    //this.userJobs.refresh();    
  }

}
