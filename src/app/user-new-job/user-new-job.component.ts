import { Component, OnInit, Input } from '@angular/core';

import {Services} from '../services/mock-services';
import {Service} from '../services/service'

import {City} from '../_models/city';
import {cities } from '../_models/mock-cities';
import {Area} from '../_models/location'
//import {MyDatePickerModule} from 'mydatepicker';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
//import { ServiceProviderService } from '../_services/serviceProvider.service';
import { ServiceProviderService} from '../services/serviceProvider.service';


//import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/common';
//import {DatePicker} from './../date-picker/datepicker';

import {MatDatepickerModule} from '@angular/material/datepicker';

import {DatePickerComponent} from '../date-picker/date-picker.component';

@Component({
  selector: 'app-user-new-job',
  templateUrl: './user-new-job.component.html',
  styleUrls: ['./user-new-job.component.css']
})
export class UserNewJobComponent implements OnInit {

  //public providedServices = Services;
  @Input() selectedService: Service;// = this.providedServices[0];
  //public providedCities= cities; //cities are defined in mock-cities.ts
  @Input() selectedCity: City;// = this.providedCities[0];
  @Input() selectedLocation: Area;// = this.selectedCity.areas[0];

  public selectedServiceName: string;// = this.providedServices[0].name;
  public selectedCityName: string;// = this.providedCities[0].name;
  public selectedLocationName: string ;//= this.selectedCity.areas[0].name;
  
  public jobTitle: string;
  public jobResponse:string;
  public date: Date;
  public jobDescription: string;
  public address: string;

  constructor ( private services: ServiceProviderService )
  {
    this.services = services; 
  }

  submit()
  {
    this.selectedServiceName = this.selectedService.name.toString();
    this.selectedCityName = this.selectedCity.name;
    this.selectedLocationName = this.selectedLocation.name;

    
    console.log("JobTitle " + this.jobTitle);
    console.log("Date " + this.date);

    console.log("city " + this.selectedCityName);
    console.log("service " + this.selectedServiceName);
    console.log("location " + this.selectedLocationName);

    if (this.jobTitle==="" || this.jobTitle==null )
    {
      this.jobResponse ="Please type something first"
      return;
    }
    //run validation check & then
    //Post a job from here
    this.services.postJob(this.selectedCityName, this.selectedLocationName, 
      this.selectedServiceName, this.jobTitle)
      .subscribe( jobPostResult => {
        var message = jobPostResult.message;
        console.log (message);
        if (jobPostResult.message==="Success" || jobPostResult.message==='success') {
          console.log(jobPostResult.result);
          this.jobResponse = "Post # "+ jobPostResult.message + " : " + jobPostResult.result.title 
        }
        else
        {
          this.jobResponse = "Post # "+ jobPostResult.message;
        }
      },err => {
          console.log("Error" + err);
          this.jobResponse = "Post # "+ err;
      }
    );
    
  }

  clearPage()
  {
    //this.address="";
    this.jobTitle="";
    this.jobResponse="";
    //this.jobDescription="";
  }

  ngOnInit() {
  }

//Obsolete code::::::::::::::::::::::::::::::::::::::::::
  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())  // Maximal selectable date
  };


  public selectedServiceIcon:String;// = this.providedServices[0].serviceIconPath;
  onServiceSelected() {
    this.selectedServiceName = this.selectedService.name.toString();
    this.selectedServiceIcon = this.selectedService.serviceIconPath;
  }  

  //public providedLocations: Area[] = this.selectedCity.areas;//= cities; //cities are defined in mock-cities.ts  
  public onCitySelected() {
    this.selectedCityName = this.selectedCity.name;
    //this.providedLocations = this.selectedCity.areas;
  }

  public onLocationSelected() {
    this.selectedLocationName = this. selectedLocation.name;
  }

}
