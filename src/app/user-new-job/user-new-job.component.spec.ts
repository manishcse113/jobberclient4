import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNewJobComponent } from './user-new-job.component';

describe('UserNewJobComponent', () => {
  let component: UserNewJobComponent;
  let fixture: ComponentFixture<UserNewJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNewJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNewJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
